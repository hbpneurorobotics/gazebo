/*
 * Copyright (C) 2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <ignition/math/Vector3.hh>

#include "gazebo/common/Assert.hh"
#include "gazebo/physics/Model.hh"
#include "plugins/InitialVelocityPlugin.hh"
#include "gazebo/physics/Joint.hh"

using namespace gazebo;

GZ_REGISTER_MODEL_PLUGIN(InitialVelocityPlugin)

/////////////////////////////////////////////////
InitialVelocityPlugin::InitialVelocityPlugin()
{
}

/////////////////////////////////////////////////
InitialVelocityPlugin::~InitialVelocityPlugin()
{
}

/////////////////////////////////////////////////
void InitialVelocityPlugin::Load(physics::ModelPtr _model,
                                 sdf::ElementPtr _sdf)
{
  GZ_ASSERT(_model, "_model pointer is NULL");
  this->model = _model;
  this->sdf = _sdf;
}

/////////////////////////////////////////////////
void InitialVelocityPlugin::Init()
{
  ModelPlugin::Init();
  // moved here for no good reason other than that
  // setXY works well here in the plugin SpringTestPlugin.
  this->Reset();
}

/////////////////////////////////////////////////
void InitialVelocityPlugin::Reset()
{
  /* If a joint tag is found then set the velocity of
   * the joint coordinates rather than that of the model dofs. */
  if (this->sdf->HasElement("joint"))
  {
    /* First get a pointer to the joint */
    auto joint_name = this->sdf->Get<std::string>("joint");
    auto joint = this->model->GetJoint(joint_name);
    if (!joint)
      gzthrow("Specified joint "+joint_name+" not found in Model");
    /* Now look for specification of axes and associated velocities. There can be two. 
       NOTE: Only rotational joints were considered when this was written. */
    for (auto elem = this->sdf->GetFirstElement(); elem; elem = elem->GetNextElement())
    {
      if (elem->GetName() == "axis")
      {
        int index = elem->Get<int>("index");
        auto param = elem->GetValue();
        if (!param)
          gzthrow("Specified joint axis but no velocity value");
        double value = 0;
        if (!param->Get(value))
          gzthrow("Invalid velocity value");
        joint->SetVelocity(index, value);
        gzdbg << "Velocity of joint "
              << joint->GetName()
              << ", axis "<< index << " = " << value << std::endl;
      }
    }
  }
  else
  {
    /* Set model dof velocities */
    if (this->sdf->HasElement("linear"))
    {
      ignition::math::Vector3d linear = this->sdf->Get<ignition::math::Vector3d>("linear");
      this->model->SetLinearVel(linear);
    }
    if (this->sdf->HasElement("angular"))
    {
      ignition::math::Vector3d angular = this->sdf->Get<ignition::math::Vector3d>("angular");
      this->model->SetAngularVel(angular);
    }
  }
}
