#!/bin/bash
set -e
set -x

# Debug printing
whoami
env | sort

# The first parameter should be location of user_scripts
. "${WORKSPACE}/user-scripts/nrp_variables"

# Prepare build
sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get install -y lcov
mkdir -p build

# Disable OpenAL support even if it is available, otherwise this can introduce ~30 second
# startup delays while pulseaudio updates cache/searches for audio devices on the system
grep -rl "include (FindOpenAL)" cmake | xargs sed -i "s/include (FindOpenAL)/set (OPENAL_FOUND FALSE)/g"
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/.local
NPROC=`nproc`
make package -j `expr $NPROC - 2`
