
# OPENSIM SPECIFIC TESTS
macro (opensim_build_tests)
  set(_append_sources TRUE)

  set(_sources)
  set(_extra_libs)

  foreach(arg ${ARGN})
    if ("${arg}" STREQUAL "EXTRA_LIBS")
      set(_append_sources FALSE)
    else()
      if (_append_sources)
        list(APPEND _sources ${arg})
      else()
        list(APPEND _extra_libs ${arg})
      endif()
    endif()
  endforeach()

  # Build all the tests
  foreach(GTEST_SOURCE_file ${_sources})
    string(REGEX REPLACE "\\.cc" "" BINARY_NAME ${GTEST_SOURCE_file})
    set(BINARY_NAME ${TEST_TYPE}_${BINARY_NAME})
    if(USE_LOW_MEMORY_TESTS)
      add_definitions(-DUSE_LOW_MEMORY_TESTS=1)
    endif(USE_LOW_MEMORY_TESTS)
    add_executable(${BINARY_NAME} ${GTEST_SOURCE_file}
                   ${GZ_BUILD_TESTS_EXTRA_EXE_SRCS})

    link_directories(${PROJECT_BINARY_DIR}/test)
    target_link_libraries(${BINARY_NAME}
      gtest
      gtest_main
      ${_extra_libs}
    )
    if (UNIX)
      # gtest uses pthread on UNIX
      target_link_libraries(${BINARY_NAME} pthread)
    endif()

    add_test(${BINARY_NAME} ${CMAKE_CURRENT_BINARY_DIR}/${BINARY_NAME}
      --gtest_output=xml:${CMAKE_BINARY_DIR}/test_results/${BINARY_NAME}.xml)

    set(_env_vars)
    list(APPEND _env_vars "CMAKE_PREFIX_PATH=${CMAKE_BINARY_DIR}:${CMAKE_PREFIX_PATH}")
    list(APPEND _env_vars "GAZEBO_PLUGIN_PATH=${CMAKE_BINARY_DIR}/plugins:${CMAKE_BINARY_DIR}/plugins/events:${CMAKE_BINARY_DIR}/plugins/rest_web")
    list(APPEND _env_vars "GAZEBO_RESOURCE_PATH=${CMAKE_SOURCE_DIR}")
    list(APPEND _env_vars "PATH=${CMAKE_BINARY_DIR}/gazebo:${CMAKE_BINARY_DIR}/tools:$ENV{PATH}")
    list(APPEND _env_vars "PKG_CONFIG_PATH=${CMAKE_BINARY_DIR}/cmake/pkgconfig:$PKG_CONFIG_PATH")
    set_tests_properties(${BINARY_NAME} PROPERTIES
      TIMEOUT 240
      ENVIRONMENT "${_env_vars}")

    # Check that the test produced a result and create a failure if it didn't.
    # Guards against crashed and timed out tests.
    add_test(check_${BINARY_NAME} ${PROJECT_SOURCE_DIR}/tools/check_test_ran.py
      ${CMAKE_BINARY_DIR}/test_results/${BINARY_NAME}.xml)

    if(GAZEBO_RUN_VALGRIND_TESTS AND VALGRIND_PROGRAM)
      add_test(memcheck_${BINARY_NAME} ${VALGRIND_PROGRAM} --leak-check=full
        --error-exitcode=1 --show-leak-kinds=all ${CMAKE_CURRENT_BINARY_DIR}/${BINARY_NAME})
    endif()
  endforeach()

  set(GZ_BUILD_TESTS_EXTRA_EXE_SRCS "")
endmacro()
# end OPENSIM SPECIFIC TESTS

