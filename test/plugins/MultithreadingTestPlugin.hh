/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef __GAZEBO_MULTITHREADING_TEST_PLUGIN_HH__


#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/Events.hh>
#include <gazebo/gazebo_config.h>
#include <gazebo/common/common.hh>
#include <gazebo/transport/transport.hh>

#include <boost/atomic.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#ifdef HAVE_OPENSIM
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimModel.hh"
#include "gazebo/physics/opensim/OpensimMuscle.hh"
#endif

namespace gazebo
{
  class MultithreadingTestPlugin : public SystemPlugin
  {
    using UniformIntDist = boost::uniform_int<int> ;
    using GeneratorType = boost::mt19937;
    using UniformInt = boost::variate_generator<GeneratorType&, UniformIntDist> ;
    using UniformReal = boost::variate_generator<GeneratorType&, boost::uniform_real<double>> ;

    public: MultithreadingTestPlugin();
    public: ~MultithreadingTestPlugin();

    public: void Load(int argc, char** argv) override;

    public: void Init() override;

    /// \brief Detect if sig-int shutdown signal is received
    private: void shutdownSignal();
    private: static std::string custom_exec(std::string _cmd);
    private: void SpawnModel(const std::string &_filename, const ignition::math::Vector3d &pos);
    private: void OnWorldCreated(std::string world_name);
    private: void RunSpawnerThread();
    private: void RunAccessorThread();
    private: void WaitTillReady();

    private: static gazebo::physics::JointPtr
      PickJoint(gazebo::physics::ModelPtr model, GeneratorType &rand_generator);
    private: static gazebo::physics::LinkPtr
      PickLink(gazebo::physics::ModelPtr model, GeneratorType &rand_generator);
    #ifdef HAVE_OPENSIM
    private: gazebo::physics::OpensimMusclePtr
      PickMuscle(gazebo::physics::OpensimModelPtr model, GeneratorType &rand_generator);
    #endif

    private: bool running_opensim_physics;
    private: boost::atomic<bool> run;
    private: physics::WorldPtr world;
    private: physics::PhysicsEnginePtr physics;
    private: boost::mutex mutex;
    private: gazebo::transport::NodePtr gazebonode;
    private: gazebo::transport::PublisherPtr factory_pub;
    private: gazebo::event::ConnectionPtr load_gazebo_ros_api_plugin_event;
    private: gazebo::event::ConnectionPtr sigint_event;
    private: boost::shared_ptr<boost::thread> spawner_thread;
    private: boost::shared_ptr<boost::thread> accessor_thread;
  };
  GZ_REGISTER_SYSTEM_PLUGIN(MultithreadingTestPlugin)
}


#endif