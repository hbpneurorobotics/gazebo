#ifndef _OPENSIMMUSCLEVISUAL_HH_
#define _OPENSIMMUSCLEVISUAL_HH_

#include <string>
#include <stack>

#include "gazebo/msgs/MessageTypes.hh"
#include "gazebo/rendering/Visual.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
  namespace rendering
  {

    class GZ_RENDERING_VISIBLE OpenSimMuscleVisual : public Visual
    {
      public:
      /// \brief Constructor
      /// \param[in] _name Name of the OpenSimMuscleVisual
      /// \param[in] _vis Pointer the parent Visual
      /// \param[in] _topicName Name of the topic which publishes the contact
      /// information.
      public: OpenSimMuscleVisual(const std::string &_name, VisualPtr _vis,
                            const std::string &_topicName);

      /// \brief Destructor
      public: virtual ~OpenSimMuscleVisual();

      public: void Fini() override;

      /// \brief Set to true to enable contact visualization.
      /// \param[in] _enabled True to show contacts, false to hide.
      public: void SetEnabled(bool _enabled);

      /// \brief Update the Visual
      ///
      /// NOTE: Shadows the Update method of the base class!
      private: void Update();

      /// \brief Callback when a Contact message is received
      /// \param[in] _msg The Contact message
      private: void OnOpenSimUpdate(ConstOpenSimMusclesPtr &_msg);

      /// \brief Create a new contact visualization point.
      private: void CreateNewMuscleVis();

      /// \brief Computes and caches the radius based on the data in the message.
      private: void ComputeFiberRadius(ConstOpenSimMusclesPtr &_msg);
      };
  }
}
#endif // _OPENSIMMUSCLEVISUAL_HH_
