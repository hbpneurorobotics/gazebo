/*
 * Copyright (C) 2012-2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/* Desc: Physics Debug Visualization Class
 * Author: Fabian Aichele
 */

#include "gazebo/common/MeshManager.hh"
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/Subscriber.hh"
#include "gazebo/msgs/msgs.hh"

#include "gazebo/rendering/ogre_gazebo.h"
#include "gazebo/rendering/Conversions.hh"
#include "gazebo/rendering/Scene.hh"
#include "gazebo/rendering/DynamicLines.hh"
#include "gazebo/rendering/ContactDebugVisualPrivate.hh"
#include "gazebo/rendering/ContactDebugVisual.hh"

using namespace gazebo;
using namespace rendering;

/////////////////////////////////////////////////
ContactDebugVisual::ContactDebugVisual(const std::string &_name, VisualPtr _vis,
                             const std::string &_topicName)
: Visual(*new ContactDebugVisualPrivate, _name, _vis)
{
  ContactDebugVisualPrivate *dPtr =
      reinterpret_cast<ContactDebugVisualPrivate *>(this->dataPtr);

  dPtr->receivedMsg = false;

  dPtr->node = transport::NodePtr(new transport::Node());
  dPtr->node->Init(dPtr->scene->Name());

  dPtr->topicName = _topicName;

  std::cout << this->Name() << ": Subscribe to topic " << dPtr->topicName << std::endl;

  dPtr->contactsSub = dPtr->node->Subscribe(dPtr->topicName,
      &ContactDebugVisual::OnContactDebug, this);

  Ogre::StringVector resourceGroups = Ogre::ResourceGroupManager::getSingletonPtr()->getResourceGroups();

  if (std::find(resourceGroups.begin(), resourceGroups.end(), "Gazebo/DebugVisual") == resourceGroups.end())
  {
      Ogre::ResourceGroupManager::getSingletonPtr()->createResourceGroup("Gazebo/DebugVisual");
  }


  dPtr->mDebugObject = new Ogre::ManualObject(this->Name() + "_ManualObject");

  dPtr->mDebugMaterial = Ogre::MaterialManager::getSingleton().create("ContactDebugVisual_Default", "Gazebo/DebugVisual");
  dPtr->mDebugMaterial->getTechnique(0)->setLightingEnabled(true);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setDiffuse(1,0,0, 1.0);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setAmbient(1,0,0);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setShininess(110);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setDepthBias(1.0f);
  dPtr->mDebugMaterial->getTechnique(0)->getPass(0)->setVertexColourTracking(Ogre::TVC_AMBIENT|Ogre::TVC_DIFFUSE|Ogre::TVC_SPECULAR|Ogre::TVC_EMISSIVE);

  _vis->GetSceneNode()->attachObject(dPtr->mDebugObject);

  dPtr->connections.push_back(
      event::Events::ConnectPreRender(
        boost::bind(&ContactDebugVisual::Update, this)));
}

/////////////////////////////////////////////////
ContactDebugVisual::~ContactDebugVisual()
{
    ContactDebugVisualPrivate *dPtr =
        reinterpret_cast<ContactDebugVisualPrivate *>(this->dataPtr);

    if (dPtr->mDebugObject)
    {
        this->GetParent()->GetSceneNode()->detachObject(dPtr->mDebugObject);
        delete dPtr->mDebugObject;
        dPtr->mDebugObject = NULL;
    }
}

/////////////////////////////////////////////////
void ContactDebugVisual::Update()
{
  ContactDebugVisualPrivate *dPtr = reinterpret_cast<ContactDebugVisualPrivate *>(this->dataPtr);

  // std::cout << "ContactDebugVisual::Update(" << this->Name() << "): dPtr->receivedMsg = " << dPtr->receivedMsg << ", dPtr->contactsMsg = " << dPtr->contactsMsg << std::endl;

  boost::mutex::scoped_lock lock(dPtr->mutex);

  if (!dPtr->contactsMsg || !dPtr->receivedMsg)
    return;

  // std::cout << "dPtr->contactsMsg->points_size() = " <<  dPtr->contactsMsg->points_size() << ", dPtr->contactsMsg->line_points_size() = " << dPtr->contactsMsg->line_points_size() << ", dPtr->contactsMsg->triangle_points_size() = " << dPtr->contactsMsg->triangle_points_size() << std::endl;

  if (dPtr->contactsMsg->points_size() > 0 || dPtr->contactsMsg->line_points_size() > 0 || dPtr->contactsMsg->triangle_points_size() > 0)
  {
      dPtr->mDebugObject->clear();

      if (dPtr->contactsMsg->line_points_size() > 0)
      {
          dPtr->mDebugObject->begin("ContactDebugVisual_Default", Ogre::RenderOperation::OT_LINE_LIST);

          // std::cout << " lines = " << dPtr->contactsMsg->line_points_size() / 2 << ": ";
          for (int k = 0; k < dPtr->contactsMsg->line_points_size(); k += 2)
          {
              //std::cout << dPtr->contactsMsg->line_points(k).x() << "," << dPtr->contactsMsg->line_points(k).y() << "," << dPtr->contactsMsg->line_points(k).z() << " - " <<
              //             dPtr->contactsMsg->line_points(k+1).x() << "," << dPtr->contactsMsg->line_points(k+1).y() << "," << dPtr->contactsMsg->line_points(k+1).z() << ";";

              Ogre::Vector3 lineBegin(dPtr->contactsMsg->line_points(k).x(), dPtr->contactsMsg->line_points(k).y(), dPtr->contactsMsg->line_points(k).z());
              Ogre::Vector3 lineEnd(dPtr->contactsMsg->line_points(k+1).x(), dPtr->contactsMsg->line_points(k+1).y(), dPtr->contactsMsg->line_points(k+1).z());

              dPtr->mDebugObject->position(lineBegin);
              dPtr->mDebugObject->colour(dPtr->contactsMsg->line_colors(k/2).x(), dPtr->contactsMsg->line_colors(k/2).y(), dPtr->contactsMsg->line_colors(k/2).z());
              dPtr->mDebugObject->position(lineEnd);
              dPtr->mDebugObject->colour(dPtr->contactsMsg->line_colors(k/2).x(), dPtr->contactsMsg->line_colors(k/2).y(), dPtr->contactsMsg->line_colors(k/2).z());
          }
          //std::cout << std::endl;
          dPtr->mDebugObject->end();
      }

      if (dPtr->contactsMsg->triangle_points_size() > 0)
      {
          dPtr->mDebugObject->begin("ContactDebugVisual_Default", Ogre::RenderOperation::OT_TRIANGLE_LIST);
          for (unsigned int k = 0; k < dPtr->contactsMsg->trianglecount(); k += 3)
          {
              Ogre::Vector3 v0(dPtr->contactsMsg->triangle_points(k).x(), dPtr->contactsMsg->triangle_points(k).y(), dPtr->contactsMsg->triangle_points(k).z());
              Ogre::Vector3 v1(dPtr->contactsMsg->triangle_points(k+1).x(), dPtr->contactsMsg->triangle_points(k+1).y(), dPtr->contactsMsg->triangle_points(k+1).z());
              Ogre::Vector3 v2(dPtr->contactsMsg->triangle_points(k+2).x(), dPtr->contactsMsg->triangle_points(k+2).y(), dPtr->contactsMsg->triangle_points(k+2).z());

              dPtr->mDebugObject->position(v0);
              dPtr->mDebugObject->colour(dPtr->contactsMsg->triangle_colors(k/3).x(), dPtr->contactsMsg->triangle_colors(k/3).y(), dPtr->contactsMsg->triangle_colors(k/3).z());
              dPtr->mDebugObject->position(v1);
              dPtr->mDebugObject->colour(dPtr->contactsMsg->triangle_colors(k/3).x(), dPtr->contactsMsg->triangle_colors(k/3).y(), dPtr->contactsMsg->triangle_colors(k/3).z());
              dPtr->mDebugObject->position(v2);
              dPtr->mDebugObject->colour(dPtr->contactsMsg->triangle_colors(k/3).x(), dPtr->contactsMsg->triangle_colors(k/3).y(), dPtr->contactsMsg->triangle_colors(k/3).z());
          }
          dPtr->mDebugObject->end();
      }

    dPtr->receivedMsg = false;
  }
}

/////////////////////////////////////////////////
void ContactDebugVisual::OnContactDebug(ConstContactDebugPtr &_msg)
{
  // std::cout << this->Name() << ": OnContactDebug() event" << std::endl;
  ContactDebugVisualPrivate *dPtr =
      reinterpret_cast<ContactDebugVisualPrivate *>(this->dataPtr);

  boost::mutex::scoped_lock lock(dPtr->mutex);
  if (dPtr->enabled)
  {
    dPtr->contactsMsg = _msg;
    dPtr->receivedMsg = true;
  }
}

/////////////////////////////////////////////////
void ContactDebugVisual::SetEnabled(bool _enabled)
{
  ContactDebugVisualPrivate *dPtr =
      reinterpret_cast<ContactDebugVisualPrivate *>(this->dataPtr);

  boost::mutex::scoped_lock lock(dPtr->mutex);

  dPtr->enabled = _enabled;

  if (!dPtr->enabled)
  {
    std::cout << this->Name() << ": Disable ContactDebugVisual message processing" << std::endl;
    dPtr->contactsSub.reset();

    dPtr->contactsMsg.reset();
    dPtr->receivedMsg = false;

    dPtr->mDebugObject->setVisible(false);
  }
  else
  {
    std::cout << this->Name() << ": ENABLE ContactDebugVisual message processing" << std::endl;
    dPtr->mDebugObject->setVisible(true);
  }

  if (!dPtr->contactsSub)
  {
    std::cout << this->Name() << ": Subscribe to topic " << dPtr->topicName << std::endl;
    dPtr->contactsSub = dPtr->node->Subscribe(dPtr->topicName,
        &ContactDebugVisual::OnContactDebug, this);
  }
}

