#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <limits>

#include "gazebo/common/MeshManager.hh"
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/Subscriber.hh"

#include "gazebo/rendering/ogre_gazebo.h"
#include "gazebo/rendering/Conversions.hh"
#include "gazebo/rendering/Scene.hh"
#include "gazebo/rendering/RenderingIface.hh"
#include "gazebo/rendering/DynamicLines.hh"
#include "gazebo/rendering/OpenSimMuscleVisualPrivate.hh"
#include "gazebo/rendering/OpenSimMuscleVisual.hh"

// silence warning for comparison between signed and unsigned types
#pragma GCC diagnostic ignored "-Wsign-compare"

#define MUSCLE_VISUALS_CALL_TRACE(x)

namespace gazebo { namespace rendering { namespace muscle_visual_internal {
static const double COLORIZATION_THRESHOLD_ACTIVATION = 0.01;
static const ignition::math::Color COLOR_INACTIVE(0, 0, 1);
static const ignition::math::Color COLOR_ACTIVE(1, 0, 0);
static const ignition::math::Color COLOR_INACTIVE_NON_MUSCLE(0, 1, 0);
}}}

using namespace gazebo;
using namespace rendering;
using namespace muscle_visual_internal;


/////////////////////////////////////////////////
// Anonymous namespace to keep this definition private to this compilation unit.
namespace {
/// @brief Compute a quaternion q for which _zdir = q * (0,0,1)
///
/// That is, the rotation represented by it rotates the base z-vector
//  so that it points along the given vector.
ignition::math::Quaterniond RotateZTo(const ignition::math::Vector3d &_zdir)
{
  using ignition::math::Vector3d;
  using ignition::math::Quaterniond;
  Vector3d norm_zdir(_zdir); norm_zdir.Normalize();
  Vector3d axis = Vector3d::UnitZ.Cross(norm_zdir);
  double angle = std::acos(norm_zdir.Z());
  return ignition::math::Quaterniond(axis, angle);
}
}

/////////////////////////////////////////////////
FiberVisualPrivate::FiberVisualPrivate() :
  last_colorization_basis_value(std::numeric_limits<float>::max()),
  last_muscle_kind{-1}
{
}

/////////////////////////////////////////////////
FiberVisual::FiberVisual(const std::string &_name, VisualPtr _parent,
                         bool _useRTShader)
  : Visual(*new FiberVisualPrivate, _name, _parent, _useRTShader)
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  // this->SetType(VT_PHYSICS);
  this->SetType(VT_GUI);
}

/////////////////////////////////////////////////
void FiberVisual::Fini()
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  FiberVisualPrivate *dPtr =
      static_cast<FiberVisualPrivate *>(this->dataPtr);

  // manual cleanup to break circular dependencies
  for (auto &vis : dPtr->attachmentPointVisual)
  {
    if (vis)
    {
      vis->Fini();
      vis.reset();
    }
  }
  for (auto &vis : dPtr->pathSegmentVisuals)
  {
    vis->Fini();
    vis.reset();
  }
  dPtr->pathSegmentVisuals.clear();

  Visual::Fini();
}

/////////////////////////////////////////////////
FiberVisual::~FiberVisual()
{
  // just checking if dtors are called!
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)
}

/////////////////////////////////////////////////
void FiberVisual::UpdateFromMsg(const msgs::OpenSimMuscle &msg, double radius)
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  // WrapObjects cause the number of nodes per patch to vary.
  // I.e. muscle fibers are dynamically subdivided to follow the
  // curvature of wrap objects.
  // Therefore we have to consider varying path lengths.

  FiberVisualPrivate *dPtr =
      static_cast<FiberVisualPrivate *>(this->dataPtr);

  // Radius is set to a fraction of some length parameters. It is constant over time.
  const int path_size = msg.pathpoint_size();
  const int path_segments_count = path_size - 1;
  bool needs_color_update = false;

  for (int i=0; i<2; ++i)
  {
    // create visual if needed
    if (dPtr->attachmentPointVisual[i] == nullptr)
    {
      std::stringstream ss; ss << "muscle_attachment_node_" << i; // child names should (must?) be unique ...
      VisualPtr vis(new Visual(ss.str(), shared_from_this(), false));
      vis->Load(); // Attaches to parent!
      /// \todo We can improve this by using instanced geometry.
      vis->AttachMesh("unit_sphere");
      vis->SetMaterial("Gazebo/Red");
      vis->SetVisibilityFlags(GZ_VISIBILITY_GUI);
      //vis->SetType(VT_GUI);
      dPtr->attachmentPointVisual[i] = vis;
      needs_color_update = true;
    }
    bool show = (i == 0 && path_size > 0) || (i == 1 && path_size > 1);
    Visual* vis = dPtr->attachmentPointVisual[i].get();
    if (show)
    {
      // put it in the right spot at the right size
      int node_idx = (i == 0) ? 0 : (path_size - 1);
      ignition::math::Vector3d pos = msgs::ConvertIgn(
        msg.pathpoint(node_idx));
      vis->SetPosition(pos);
      vis->SetScale(ignition::math::Vector3d(radius, radius, radius));
    }
    if (vis->GetVisible() != show) // cheap
    {
      vis->SetVisible(show); // might be expensive
      needs_color_update |= show;
    }
  }

  int i = 0;
  for (; i<path_segments_count; ++i)
  {
    // create visual if needed
    if (dPtr->pathSegmentVisuals.size() <= i)
    {
      std::stringstream ss; ss << "muscle_segment_node_" << i;
      VisualPtr vis(new Visual(ss.str(), shared_from_this(), false));
      vis->Load(); // Attaches to parent!
      vis->AttachMesh("unit_cylinder");
      vis->SetMaterial("Gazebo/Red");
      vis->SetVisibilityFlags(GZ_VISIBILITY_GUI);
      //vis->SetType(VT_GUI);
      dPtr->pathSegmentVisuals.push_back(vis);
      needs_color_update = true;
    }
    // Put it in the right spot. Align local z-direction
    // with the path segment relative to world.
    Visual* vis = dPtr->pathSegmentVisuals[i].get();
    ignition::math::Vector3d pos1 = msgs::ConvertIgn(msg.pathpoint(i));
    ignition::math::Vector3d pos2 = msgs::ConvertIgn(msg.pathpoint(i+1));
    const double length = (pos2-pos1).Length();
    ignition::math::Pose3d pose(pos1, RotateZTo(pos2 - pos1));
    // Add some local offset since the cylinder reaches from z = -length/2 to length/2
    // The multiplication from the left is opposed to mathematical convention, but whatever, it works ...
    pose = ignition::math::Pose3d(ignition::math::Vector3d(0,0,0.5 * length), ignition::math::Quaterniond()) * pose;
    ignition::math::Vector3d scale(radius, radius, length);
    vis->SetPose(pose);
    vis->SetScale(scale);
    if (vis->GetVisible() == false) // cheap
    {
      vis->SetVisible(true); // might be expensive
      needs_color_update = true;
    }
  }

  // Turn off superflous segments in case there are some.
  // We might also just remove them, but this is rather costly.
  for (; i<dPtr->pathSegmentVisuals.size(); ++i)
  {
    Visual* vis = dPtr->pathSegmentVisuals[i].get();
    if (vis->GetVisible() == true) // cheap
      vis->SetVisible(false); // might be expensive
  }

  const float activation = std::min(1., std::max(0., msg.activation()));
  needs_color_update |= std::abs(activation - dPtr->last_colorization_basis_value) > COLORIZATION_THRESHOLD_ACTIVATION;
  needs_color_update |= dPtr->last_muscle_kind != (int)msg.ismuscle();
  ignition::math::Color color;
  if (needs_color_update)
  {
    dPtr->last_colorization_basis_value = activation;
    dPtr->last_muscle_kind = (int)msg.ismuscle();
    ignition::math::Color actual_color_inactive = msg.ismuscle() ? COLOR_INACTIVE : COLOR_INACTIVE_NON_MUSCLE;
    color = COLOR_ACTIVE * activation + actual_color_inactive * (1. - activation);
    this->SetDiffuse(color); // look pretty expensive
    this->SetAmbient(color);
  }
}

/////////////////////////////////////////////////
OpenSimMuscleVisual::OpenSimMuscleVisual(const std::string &_name, VisualPtr _vis,
                                         const std::string &_topicName)
  : Visual(*new OpenSimMuscleVisualPrivate, _name, _vis)
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  dPtr->type = VT_VISUAL;
  dPtr->receivedMsg = false;
  dPtr->node = transport::NodePtr(new transport::Node());
  dPtr->node->Init(dPtr->scene->Name());
  dPtr->fiber_radius = 0.;
  dPtr->last_muscle_count = 0;

  std::cout << "OpenSimMuscleVisual subscribing to topic: " << _topicName << std::endl;

  dPtr->topicName = _topicName;
  dPtr->openSimSub = dPtr->node->Subscribe(dPtr->topicName,
                                           &OpenSimMuscleVisual::OnOpenSimUpdate, this);

  dPtr->connections.push_back(
        event::Events::ConnectPreRender(
          boost::bind(&OpenSimMuscleVisual::Update, this)));
}

/////////////////////////////////////////////////
OpenSimMuscleVisual::~OpenSimMuscleVisual()
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::Fini()
{
  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  for (auto vis : dPtr->muscle_visuals)
    vis->Fini();
  dPtr->muscle_visuals.clear();

  Visual::Fini();
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::Update()
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  boost::mutex::scoped_lock lock(dPtr->mutex);

  if (!dPtr->openSimMsg || !dPtr->receivedMsg)
    return;

  if (dPtr->fiber_radius <= 0. || dPtr->last_muscle_count != dPtr->openSimMsg->muscle_size())
  {
    ComputeFiberRadius(dPtr->openSimMsg);
    dPtr->last_muscle_count = dPtr->openSimMsg->muscle_size();
  }

  try
  {
    for (int i = 0; i < dPtr->openSimMsg->muscle_size(); i++)
    {
      if (dPtr->muscle_visuals.size() <= i)
        this->CreateNewMuscleVis();
      dPtr->muscle_visuals[i]->UpdateFromMsg(dPtr->openSimMsg->muscle(i), dPtr->fiber_radius);
    }
    // Get rid of superlous visuals.
    while (dPtr->muscle_visuals.size() > dPtr->openSimMsg->muscle_size())
    {
      this->DetachVisual(dPtr->muscle_visuals.back());
      dPtr->muscle_visuals.back()->Fini();
      dPtr->muscle_visuals.pop_back();
    }
  }
  catch (Ogre::Exception& ex)
  {
    std::cout << "Exception in OpenSimMuscleVisual: " << ex.what() << std::endl;
  }

  dPtr->receivedMsg = false;
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::ComputeFiberRadius(ConstOpenSimMusclesPtr &_msg)
{
  auto *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);
  double total_length = 0.;
  int num_muscles = _msg->muscle_size();
  for (int i = 0; i < num_muscles; i++)
  {
    total_length += _msg->muscle(i).length();
  }
  dPtr->fiber_radius = 0.05*total_length/num_muscles;
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::OnOpenSimUpdate(ConstOpenSimMusclesPtr &_msg)
{
  //MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  boost::mutex::scoped_lock lock(dPtr->mutex);
  if (dPtr->enabled)
  {
    dPtr->openSimMsg = _msg;
    dPtr->receivedMsg = true;

    //std::cout << "OpenSimMuscleVisual received update message: " << dPtr->openSimMsg->muscle_size() << " muscles." << std::endl;
  }
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::SetEnabled(bool _enabled)
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  boost::mutex::scoped_lock lock(dPtr->mutex);

  dPtr->enabled = _enabled;

  if (!dPtr->enabled)
  {
    dPtr->openSimSub.reset();
    dPtr->openSimMsg.reset();
    dPtr->receivedMsg = false;

    for (unsigned int c = 0 ; c < dPtr->muscle_visuals.size(); c++)
      dPtr->muscle_visuals[c]->SetVisible(false);
  }
  else if (!dPtr->openSimSub)
  {
    dPtr->openSimSub = dPtr->node->Subscribe(dPtr->topicName,
                                              &OpenSimMuscleVisual::OnOpenSimUpdate, this);
  }
}

/////////////////////////////////////////////////
void OpenSimMuscleVisual::CreateNewMuscleVis()
{
  MUSCLE_VISUALS_CALL_TRACE(gzdbg << __FUNCTION__ << std::endl;)

  OpenSimMuscleVisualPrivate *dPtr =
      static_cast<OpenSimMuscleVisualPrivate *>(this->dataPtr);

  std::string objName = this->Name() +
      "_muscle_" + std::to_string(dPtr->muscle_visuals.size());

  auto muscleVis =
      std::make_shared<FiberVisual>(objName, shared_from_this(), false);
  muscleVis->Load();

  dPtr->muscle_visuals.push_back(muscleVis);
}
