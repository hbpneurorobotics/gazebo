/***************************************************************************

This source file is part of OGREBULLET
(Object-oriented Graphics Rendering Engine Bullet Wrapper)

Copyright (c) 2007 tuan.kuranes@gmail.com (Use it Freely, even Statically, but have to contribute any changes)



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "gazebo/physics/OgreDebugLines.hh"

using namespace gazebo::physics;
using namespace Ogre;

//------------------------------------------------------------------------------------------------
bool DebugLines::_materials_created = false;
//------------------------------------------------------------------------------------------------
DebugLines::DebugLines(Ogre::SceneManager *scm)
{
    _drawn = false;
    _active = false;

    std::cout << "DebugLines::DebugLines(" << scm << ")" << std::endl;

    mPoints = new Ogre::ManualObject("DebugLines_mPoints");
    mLines = new Ogre::ManualObject("DebugLines_mLines");

    if (!_materials_created)
    {
        StringVector resourceGroups = ResourceGroupManager::getSingletonPtr()->getResourceGroups();

        if (std::find(resourceGroups.begin(), resourceGroups.end(), "OgreBulletCollisions") == resourceGroups.end())
        {
            ResourceGroupManager::getSingletonPtr()->createResourceGroup("OgreBulletCollisions");
        }

        MaterialPtr red = MaterialManager::getSingleton().create("OgreBulletCollisionsDebugLines/Disabled","OgreBulletCollisions");
        MaterialPtr green = MaterialManager::getSingleton().create("OgreBulletCollisionsDebugLines/Enabled","OgreBulletCollisions");
        MaterialPtr blue = MaterialManager::getSingleton().create("OgreBulletCollisionsDebugLines/Static","OgreBulletCollisions");

		MaterialPtr redVisible = MaterialManager::getSingleton().create("OgreBulletCollisionsDebugLines/RED","OgreBulletCollisions");

        red->setReceiveShadows(false);
        red->getTechnique(0)->setLightingEnabled(true);
		red->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0);
		red->getTechnique(0)->getPass(0)->setDiffuse(1,0,0, 1.0);
		red->getTechnique(0)->getPass(0)->setAmbient(1,0,0 );
		red->getTechnique(0)->getPass(0)->setShininess(110);
		red->getTechnique(0)->getPass(0)->setDepthBias(1.0f);

        red->setLightingEnabled(true);

		redVisible->setReceiveShadows(false);
		redVisible->getTechnique(0)->setLightingEnabled(true);
		redVisible->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0);
		redVisible->getTechnique(0)->getPass(0)->setDiffuse(1,0,0, 1.0);
		redVisible->getTechnique(0)->getPass(0)->setAmbient(1,0,0 );
		redVisible->getTechnique(0)->getPass(0)->setShininess(110);
		redVisible->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
		redVisible->getTechnique(0)->getPass(0)->setDepthWriteEnabled(true);
        redVisible->setLightingEnabled(true);


        green->setReceiveShadows(false);
        green->getTechnique(0)->setLightingEnabled(true);
		green->getTechnique(0)->getPass(0)->setSelfIllumination(0,1,0);
		green->getTechnique(0)->getPass(0)->setDiffuse(0,1,0, 1.0);
		green->getTechnique(0)->getPass(0)->setAmbient(0,1,0 );
		green->getTechnique(0)->getPass(0)->setShininess(110);
		green->getTechnique(0)->getPass(0)->setDepthBias(1.0f);

        green->setLightingEnabled(true);

        blue->setReceiveShadows(false);
        blue->getTechnique(0)->setLightingEnabled(true);
		blue->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1);
		blue->getTechnique(0)->getPass(0)->setDiffuse(0,0,1, 1.0);
		blue->getTechnique(0)->getPass(0)->setAmbient(0,0,1);
		blue->getTechnique(0)->getPass(0)->setShininess(110);
		blue->getTechnique(0)->getPass(0)->setDepthBias(1.0f);

        blue->setLightingEnabled(true);

        _materials_created = true;
    }
    mPoints->setCastShadows(false);
    mLines->setCastShadows(false);

    mLines->begin("OgreBulletCollisionsDebugLines/RED", RenderOperation::OT_LINE_LIST);
    mLines->position( Vector3::ZERO );
    mLines->colour( ColourValue::Blue );
    mLines->position( Vector3::ZERO );
    mLines->colour( ColourValue::Blue );

    mPoints->begin("OgreBulletCollisionsDebugLines/Enabled", RenderOperation::OT_POINT_LIST);
    mPoints->position( Vector3::ZERO );

    mSceneManager = scm;
    std::cout << "mSceneManager = " << mSceneManager << std::endl;
    if (mSceneManager)
    {
        std::cout << " attach mPoints & mLines to mSceneManager" << std::endl;
        mDebugParentNode = mSceneManager->getRootSceneNode()->createChildSceneNode("DebugBulletPhysics_ParentNode");
        mDebugParentNode->attachObject(mPoints);
        mDebugParentNode->attachObject(mLines);
    }
}


//------------------------------------------------------------------------------------------------
void DebugLines::clear(bool force)
{
    //std::cout << "DebugLines::clear(): _drawn = " << _drawn << std::endl;
    if (_drawn || force)
    {
        _drawn = false;
        _points.clear();
        _lines.clear();
        _triangles.clear();

        _pointColors.clear();
        _lineColors.clear();
        _triangleColors.clear();
    }
}
//------------------------------------------------------------------------------------------------
DebugLines::~DebugLines()
{ 
    clear();

    if (mSceneManager != NULL)
        mSceneManager->getRootSceneNode()->removeAndDestroyChild("DebugBulletPhysics_ParentNode");

    if (mPoints)
        delete mPoints;

    if (mLines)
        delete mLines;
}

void DebugLines::beginDraw()
{
    std::cout << "DebugLines::beginDraw()" << std::endl;
    mPoints->beginUpdate(0);
    mLines->beginUpdate(0);
}

//------------------------------------------------------------------------------------------------
void DebugLines::draw()
{
    std::cout << "DebugLines::draw(): _drawn = " << _drawn << ", _points.size() = " << _points.size() << std::endl;

    if (!_active)
    {
        std::cout << " not active, returning" << std::endl;

        // _points.clear();
        // _lines.clear();

        return;
    }
    if (_drawn || (_points.empty() && _lines.empty()))
    {
        std::cout << " _drawn = true, returning" << std::endl;
        return;
    }
    else
    {
        _drawn = true;


        for (unsigned int k = 0; k < _points.size(); k++)
        {
            mPoints->position(_points[k]);
            mPoints->colour(_pointColors[k].x, _pointColors[k].y, _pointColors[k].z, 0.5f);
        }

        for (unsigned int k = 0; k < _lines.size(); k += 2)
        {
            std::cout << " line " << k << " color = " << _lineColors[k/2] << std::endl;
            mLines->position(_lines[k]);
            mLines->colour(_lineColors[k/2].x, _lineColors[k/2].y, _lineColors[k/2].z, 1.0f);
            mLines->position(_lines[k + 1]);
            mLines->colour(_lineColors[k/2].x, _lineColors[k/2].y, _lineColors[k/2].z, 1.0f);
        }

    }
}

void DebugLines::endDraw()
{
    std::cout << "DebugLines::endDraw()" << std::endl;
    mPoints->end();
    mLines->end();
}
