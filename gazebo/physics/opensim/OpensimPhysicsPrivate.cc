/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"
#include "gazebo/physics/opensim/OpensimMeshShape.hh"
#include "gazebo/physics/opensim/OpensimPlaneShape.hh"
#include "gazebo/physics/opensim/OpensimSphereShape.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimModel.hh"
#include "gazebo/physics/OgreDebugDrawer.hh"

#include "gazebo/common/Assert.hh"
#include "gazebo/physics/World.hh"

#include <cstdio>
#include <SimTKcommon/internal/Xml.h>
#include <unordered_map>

namespace gazebo { namespace physics {

Mob::Mob()
  : gzJoint(nullptr), gzLink(nullptr)
{
  memset(this->initial_state, 0, sizeof(this->initial_state));
}

//////////////////////////////////////////////////
/// OpensimPhysics Members
//////////////////////////////////////////////////

//////////////////////////////////////////////////
OpensimPhysicsPrivate::OpensimPhysicsPrivate(WorldPtr _world, OpensimPhysics *_engine)
  : engine(_engine),
    unique_name_counter(0),
    integratorAccuracy(1.e-2),
    integratorMinStepSize(0.),
    contactForceManager(this)
{
  this->osimManager.setModel(this->osimModel);
  this->osimModel.updBodySet().setMemoryOwner(false);
  this->osimModel.updForceSet().setMemoryOwner(false);
  this->osimModel.updContactGeometrySet().setMemoryOwner(false);
  this->osimModel.updControllerSet().setMemoryOwner(false);
  this->osimModel.updConstraintSet().setMemoryOwner(false);
  OpenSim::Object::clearArrayPropertiesOnUpdateFromXML = false;
  OpenSim::Object::mergeObjectWithSameNameFromXML = true; // Because wrap objects are defined under body tags.
  this->osimModel.setUseVisualizer(false);

#ifdef OPENSIM_USE_DEBUG_DRAWER
  this->debugDrawer = new DebugDrawer(NULL);
  this->debugDrawer->setDrawContactPoints(true);
  this->debugDrawer->setDrawWireframe(true);
#endif //OPENSIM_USE_DEBUG_DRAWER
}


void OpensimPhysicsPrivate::Load(sdf::ElementPtr _sdf)
{
  sdf::ElementPtr opensimElem = _sdf->GetElement("opensim");
  if (opensimElem == nullptr)
    gzthrow("Cannot get the opensim element from the sdf.");

  // Get default values from sdf spec if none are specified in the model sdf.
  this->integratorType = opensimElem->Get<std::string>("integrator");
  this->integratorAccuracy = opensimElem->Get<double>("accuracy");
  this->integratorMinStepSize = opensimElem->Get<double>("min_step_size");

  double initialTime = this->engine->World()->SimTime().Double(); // You never know ...
  this->osimManager.setInitialTime(initialTime);
  this->osimManager.setFinalTime(initialTime);
}


//////////////////////////////////////////////////
void OpensimPhysicsPrivate::RegenerateIntegrator()
{
  auto& system = this->osimModel.updMultibodySystem();
  if (this->integratorType == "rk_merson")
    this->integrator = boost::make_shared<SimTK::RungeKuttaMersonIntegrator>(system);
  else if (this->integratorType == "rk3")
    this->integrator = boost::make_shared<SimTK::RungeKutta3Integrator>(system);
  else if (this->integratorType == "rk2")
    this->integrator = boost::make_shared<SimTK::RungeKutta2Integrator>(system);
  else if (this->integratorType == "semi_explicit_euler")
    this->integrator = boost::make_shared<SimTK::SemiExplicitEuler2Integrator>(system);
  else if (this->integratorType == "semi_explicit_euler_fixed")
    this->integrator = boost::make_shared<SimTK::SemiExplicitEulerIntegrator>(system, this->engine->GetMaxStepSize());
  else
  {
    gzmsg << "Integrator type not specified, using SemiExplicitEuler2Integrator with default accuracy of " << this->integratorAccuracy << "\n";
    this->integrator = boost::make_shared<SimTK::SemiExplicitEuler2Integrator>(system);
    return;
  }
  this->integrator->setAccuracy(this->integratorAccuracy);
  this->integrator->setMaximumStepSize(this->engine->GetMaxStepSize());
  this->integrator->setMinimumStepSize(integratorMinStepSize);
  this->osimManager.setIntegrator(this->integrator.get());
}


//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::CreateUniquePostfix()
{
  char buffer[2 * sizeof(this->unique_name_counter)+2];
  std::snprintf(buffer, sizeof(buffer), "_%lx", this->unique_name_counter);
  this->unique_name_counter ++;
  return std::string{buffer};
}

//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::CreateUniqueOsimNameFor(const Base *obj)
{
#if (ASSIGN_UNIQUE_OPENSIM_OBJECT_NAMES)
  return obj->GetName() + CreateUniquePostfix();
#else
  return obj->GetName();
#endif
}

//////////////////////////////////////////////////
namespace muscle_loading_detail
{

using Element = SimTK::Xml::Element;

static std::string FullElementPath(Element el)
{
  std::stringstream ss;
  std::vector<Element> parents; parents.reserve(64);
  while (true)
  {
    parents.push_back(el);
    if (!el.hasParentElement())
      break;
    else
      el = el.getParentElement();
  }
  for (int i = parents.size()-1; i>=0; --i)
  {
    ss << '<' << parents[i].getNodeText() << '>';
  }
  return ss.str();
}


class XmlForceNameBender
{
  using NameMappingFunction = std::function<std::string (const std::string &)>;
  NameMappingFunction name_mapping_function;

  bool IsActuatorDefinition(Element &el) const
  {
    if (el.isValueElement() || !el.hasAttribute("name")) return false;
    if (!el.hasParentElement() || !el.getParentElement().hasParentElement() ||
        el.getParentElement().getParentElement().getNodeText() != "ForceSet")
      return false;
    auto name = el.getNodeText();
    return OpenSim::Object::isObjectTypeDerivedFrom<OpenSim::Force>(name);
  }

  bool IsPathPoint(Element &el) const
  {
    return !el.isValueElement() && el.getNodeText() == "PathPoint" && el.hasAttribute("name");
  }

  void GiveNewNameTo(Element &el) const
  {
    auto current_name = el.getRequiredAttributeValue("name");
    auto new_name = name_mapping_function(current_name);
    OPENSIM_MUSCLE_DEBUG(
      gzdbg << "In " << FullElementPath(el) << ": " << current_name << " -> " << new_name << std::endl;
    )
    el.setAttributeValue("name", new_name);
  }

public:
    XmlForceNameBender(NameMappingFunction _name_mapping_function)
    : name_mapping_function(_name_mapping_function)
  {}

  void AdjustMuscleNameIn(Element &el)
  {
    if (IsActuatorDefinition(el) || IsPathPoint(el))
      GiveNewNameTo(el);
  }
};


class XmlBodyNameBender
{
  // Name references in <body> tags will be replaced with the name of associated simbody bodies.
  // This is necessary because we want the user to specify names w.r.t. to gazebo link names.
  // The link names are in general not equal to the names of the associated opensim bodies!

  typedef std::unordered_map<std::string, std::pair<std::string, bool>> NameMap;
  // ----------------------------------------------------
  // Key is the original link name. Value is tuple of mangled body name, and flag indicating if the name is used more than once in the model.
  NameMap gazebo_to_osim_name;
  const OpensimModel *model;
  // ----------------------------------------------------

  std::string MapBodyName(std::string old_name, const Element &el) const
  {
    auto full_el_path = FullElementPath(el);
    auto it = gazebo_to_osim_name.find(old_name);
    if (it == gazebo_to_osim_name.end())
    {
      gzthrow("Body name " + old_name + ", referenced by (" + full_el_path + ") was not found in model " + model->GetName());
    }
    else if (it->second.second == false)
    {
      gzthrow("Body name " + old_name + " referenced by (" + full_el_path + ") is used multiple times in model " + model->GetName() + ". Ambiguous references not allowed.");
    }
    std::string new_name = it->second.first;
    OPENSIM_MUSCLE_DEBUG(
      gzdbg << "In " << full_el_path << ": " << old_name << " -> " << new_name << std::endl;
    )
    return new_name;
  }

  bool IsElementReferencingABody(const Element &el) const
  {
    return el.isValueElement() && el.getNodeText() == "body";
  }

  bool IsElementABodyDefinition(Element &el) const
  {
    bool ret = !el.isValueElement() && el.getNodeText() == "Body" && el.hasAttribute("name");
    GZ_ASSERT(ret==false ||
      (el.hasParentElement() && el.getParentElement().hasParentElement() &&
       (el.getParentElement().getParentElement().getNodeText() == "BodySet")), "Bending a body name not in a BodySet?!");
    return ret;
  }

public:
  XmlBodyNameBender(const OpenSim::Model &osimModel,
                const OpensimModel &_model,
                std::function<std::string(OpensimLink&)> getOsimNameOf)
    : model{&_model}
  {
    // Special case handling for the world/ground reference frame. (Assignment uses c++11 initializer lists.)
    gazebo_to_osim_name["world"] = { osimModel.getGroundBody().getName(), true };
    // Add names of the links of this model in a map for quick lookups.
    const physics::Link_V &links = _model.GetLinks();
    for (const LinkPtr &base_link : links)
    {
      OpensimLinkPtr link = boost::static_pointer_cast<OpensimLink>(base_link);
      auto it = gazebo_to_osim_name.find(link->GetName());
      if (it == gazebo_to_osim_name.end())
      {
        gazebo_to_osim_name[link->GetName()] = { getOsimNameOf(*link), true };
      }
      else
      {
        it->second.second = false; // False indicating bad things happing.
      }
    }
  }


  void AdjustBodyNameIn(Element &el)
  {
    if (IsElementReferencingABody(el))
    {
      // Encountered in geometry path
      el.setValue(MapBodyName(el.getValue(), el));
    }
    if (IsElementABodyDefinition(el))
    {
      // Encountered in BodySet. In case we encounter a not anticipated situation we trigger an assertion.
      el.setAttributeValue("name", MapBodyName(el.getRequiredAttributeValue("name"), el));
    }
  }
};


void VisitElements(
  SimTK::Xml::Element &_root,
  std::function<void (Element &)> visitor)
{
  // Traverse XML tree using an explict stack.
  std::vector<Element> stack;
  stack.push_back(_root);
  while (!stack.empty())
  {
    Element el = stack.back(); stack.pop_back();
    visitor(el);
    if (!el.isValueElement())
    {
      // Add children to stack.
      SimTK::Array_<SimTK::Xml::Element> children = el.getAllElements();
      for (SimTK::Xml::Element &c : children)
      {
        stack.push_back(c);
      }
    }
  }
}


void MoveGroundedPathPointsByModelPose(OpenSim::PathActuator &actuator, const OpensimModel &_model, const OpensimPhysicsPrivate &engine_priv)
{
  const std::string opensim_ground_body_name = engine_priv.osimModel.getGroundBody().getName();
  auto &geompath = actuator.updGeometryPath();
  auto &pathpointset = geompath.updPathPointSet();
  for (int i=0; i<pathpointset.getSize(); ++i)
  {
    auto &pp = pathpointset[i];
    if (pp.getBodyName() == opensim_ground_body_name)
    {
      SimTK::Transform Xwm = opensim_internal::Pose2Transform(_model.WorldPose());
      SimTK::Vec3 p = Xwm * pp.getLocation();
      for (int k = 0; k < 3; ++k)
        pp.setLocationCoord(k, p[k]);
    }
  }
}

} // namespace



void OpensimPhysicsPrivate::LoadMuscleDefinitionsAndDoSanityCheck(OpensimModel &_model)
{
  const std::string fn = _model.muscle_definition_filename;
  if (fn.empty()) return;

  OpenSim::Model &om = this->osimModel;

  // Track and check how many of each objects were added.
  // Also track the forces that were in  the beginning in the model.
  int num_forces = om.getForceSet().getSize();
  int new_forces_idx_start = num_forces;
  int num_bodies = om.getBodySet().getSize();
  int num_joints = om.getJointSet().getSize();
  int num_geometries = om.getContactGeometrySet().getSize();
  std::vector<const OpenSim::Force*> previous_forces; previous_forces.reserve(num_forces);
  for (int i=0; i<num_forces; ++i)
    previous_forces.push_back(&om.getForceSet()[i]);

  auto link_name_mapping_function = [this](const OpensimLink &l) -> std::string
  {
    return this->GetOsimNameFor(&l);
  };

  std::unordered_map<std::string, std::string> opensim_actuator_name_to_gz_name;
  auto muscle_name_mapping_function =
    [this, &opensim_actuator_name_to_gz_name](const std::string &original) -> std::string
  {
    std::string ret = original+this->CreateUniquePostfix();
        opensim_actuator_name_to_gz_name[ret] = original;
    return ret;
  };

  gzmsg << "Loading muscle defintions from file " << fn << "." << std::endl;
  {
    auto document = boost::make_shared<OpenSim::XMLDocument>(fn);
    SimTK::Xml::Element myNode =  document->getRootDataElement(); //either actual root or node after OpenSimDocument
    muscle_loading_detail::XmlBodyNameBender bodynamebender(this->osimModel, _model, link_name_mapping_function);
    muscle_loading_detail::XmlForceNameBender actuatornamebender(muscle_name_mapping_function);
    muscle_loading_detail::VisitElements(
        myNode,
        [&bodynamebender, &actuatornamebender](muscle_loading_detail::Element &el)
        {
          bodynamebender.AdjustBodyNameIn(el);
          actuatornamebender.AdjustMuscleNameIn(el);
        }
    );
    this->osimModel.updateFromXMLNode(myNode, document->getDocumentVersion());
    this->osimModel.updMuscles(); // Has the side effect of updating the array of muscles from the array of forces.
  }

  if (om.getForceSet().getSize() < new_forces_idx_start)
    gzthrow ("Loading actuator definitions should not decrease the number of muscles in the system!");
  for (int i=0; i<num_forces; ++i)
  {
    if (previous_forces[i] != &om.getForceSet()[i])
      gzthrow("Loading muscle definitions changed the order in which forces appear in the forces array. This was not supposed to happen!");
  }
  if ((om.getBodySet().getSize() != num_bodies) ||
      (om.getJointSet().getSize() != num_joints) ||
      (om.getContactGeometrySet().getSize() != num_geometries))
    gzthrow("Loading of other items except for muscles is not supported!");

  gzmsg << "Found new forces:" << std::endl;
  for (int idx = new_forces_idx_start; idx < om.getForceSet().getSize(); ++idx)
  {
    OpenSim::Force &osim_f = om.getForceSet()[idx];
    _model.owned_osim_forces.push_back(boost::shared_ptr<OpenSim::Force>(&osim_f));
    auto gzname = opensim_actuator_name_to_gz_name[osim_f.getName()];
    gzmsg << "  " << gzname << "(" << osim_f.getName() << ")[" << osim_f.getConcreteClassName() << "]" << std::endl;
    if (auto *pa = dynamic_cast<OpenSim::PathActuator*>(&osim_f))
    {
      muscle_loading_detail::MoveGroundedPathPointsByModelPose(*pa, _model, *this);
      auto m = boost::make_shared<OpensimPathActuator>(pa, gzname, *this->engine);
      _model.muscles.push_back(m);
    }
  }
}


//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::GetOsimNameFor(const OpensimSphereShape *obj) const
{
  return obj->osimGeometry->getName();
}

//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::GetOsimNameFor(const OpensimPlaneShape *obj) const
{
  return obj->osimGeometry->getName();
}

//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::GetOsimNameFor(const OpensimMeshShape *obj) const
{
  return obj->osimGeometry->getName();
}

//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::GetOsimNameFor(const OpensimCollision *obj) const
{
  return obj->osimGeometry->getName();
}

//////////////////////////////////////////////////
std::string OpensimPhysicsPrivate::GetOsimNameFor(const OpensimLink *obj) const
{
  return obj->GetPriv()->GetOsimName();
}

//////////////////////////////////////////////////
const SimTK::State& OpensimPhysicsPrivate::getWorkingStateRealizedToStage(SimTK::Stage stage) const
{
  const SimTK::State &state = this->osimModel.getWorkingState();
  if (state.getSystemStage() < stage)
  {
    this->osimModel.getMultibodySystem().realize(state, stage);
  }
  return state;
}


//////////////////////////////////////////////////
SimTK::State& OpensimPhysicsPrivate::updWorkingStateRealizedToStage(SimTK::Stage stage)
{
  SimTK::State &state = this->osimModel.updWorkingState();
  if (state.getSystemStage() < stage)
  {
    this->osimModel.getMultibodySystem().realize(state, stage);
  }
  return state;
}

//////////////////////////////////////////////////
/// OpensimLink Members
//////////////////////////////////////////////////

//////////////////////////////////////////////////
OpenSim::Body& OpensimLinkPrivate::getOsimBody()
{
  GZ_ASSERT(isOsimRepresentationInitialized(),
            "Opensim repr. of this link must be initialized");
  if (master_index >= 0)
  {
    GZ_ASSERT(this->mobs[this->master_index].body != nullptr, "Mob must be initialized with body reference at this point.");
    return *this->mobs[this->master_index].body;
  }
  else
    return engine->GetPriv()->osimModel.getGroundBody();
}

//////////////////////////////////////////////////
SimTK::MobilizedBody& OpensimLinkPrivate::getSimTKBody()
{
  return engine->GetPriv()->osimModel.updMatterSubsystem().updMobilizedBody(
    this->getOsimBody().getIndex());
}

//////////////////////////////////////////////////
const SimTK::MobilizedBody& OpensimLinkPrivate::getSimTKBody() const
{
  return engine->GetPriv()->osimModel.getMatterSubsystem().getMobilizedBody(
    const_cast<OpensimLinkPrivate*>(this)->getOsimBody().getIndex());
}

////////////////////////////////////////////////////
const SimTK::Force::DiscreteForces &OpensimLinkPrivate::getDiscreteForces() const {
  return engine->GetPriv()->discreteForces;
}

//////////////////////////////////////////////////
SimTK::Transform OpensimLinkPrivate::getSimTkTransform(const SimTK::State &state) const
{
  const auto &matter_subsystem = engine->GetPriv()->osimModel.getMatterSubsystem();
  const SimTK::MobilizedBodyIndex body_idx = const_cast<OpensimLinkPrivate*>(this)->getOsimBody().getIndex();
  const auto &mob = matter_subsystem.getMobilizedBody(body_idx);
  return mob.getBodyTransform(state);
}

//////////////////////////////////////////////////
void OpensimLinkPrivate::SaveCoordinatesInDefaultValues(const SimTK::State &state)
{
  for (Mob &mob : this->mobs)
  {
    GZ_ASSERT(mob.joint != nullptr && mob.body != nullptr,
              "OpenSim pointers should not be null!");
    OpenSim::CoordinateSet& jc = mob.joint->upd_CoordinateSet();
    for (int i=0; i<mob.joint->numCoordinates(); ++i)
    {
      jc[i].setDefaultValue(jc[i].getValue(state));
      jc[i].setDefaultSpeedValue(jc[i].getSpeedValue(state));
    }
  }
}

//////////////////////////////////////////////////
void OpensimLinkPrivate::SaveInitialCoordinates()
{
  for (Mob &mob : this->mobs)
  {
    GZ_ASSERT(mob.joint != nullptr && mob.body != nullptr,
              "OpenSim pointers should not be null!");
    // Want to remember the initial state for when the world is reset.
    const OpenSim::CoordinateSet& coordinateSet = mob.joint->get_CoordinateSet();
    GZ_ASSERT(coordinateSet.getSize() <= 6, "Can there be more than 6 DOF's for a joint?");
    const int n = std::min(6, coordinateSet.getSize());
    for (int i=0; i<n; ++i)
    {
      const OpenSim::Coordinate &c = coordinateSet[i];
      mob.initial_state[i] = c.getDefaultValue();
    }
  }
}

//////////////////////////////////////////////////
void OpensimLinkPrivate::RestoreDefaultValuesToInitialCoordinates()
{
  for (Mob &mob : this->mobs)
  {
    GZ_ASSERT(mob.joint != nullptr && mob.body != nullptr,
              "OpenSim pointers should not be null!");
    OpenSim::CoordinateSet& coordinateSet = mob.joint->upd_CoordinateSet();
    GZ_ASSERT(coordinateSet.getSize() <= 6, "Can there be more than 6 DOF's for a joint?");
    const int n = std::min(6, coordinateSet.getSize());
    for (int i=0; i<n; ++i)
    {
      OpenSim::Coordinate &c = coordinateSet[i];
      c.setDefaultValue(mob.initial_state[i]);
    }
  }
}

//////////////////////////////////////////////////
void OpensimLinkPrivate::AddSlaveMasterWeldConstraintsTo(OpenSim::Model &_osimModel)
{
    // Secondly, create weld constraints between slaves and their master.
    if (this->mobs.size() > 1)
    {
      for (unsigned i = 0; i < this->mobs.size(); ++i)
      {
        if ((int)i == this->master_index)
          continue;
        Mob *slave = &this->mobs[i];
        Mob* master =&this->mobs[this->master_index];
        gzdbg << "Add constraint between '" << master->body->getName() << "' and '" << slave->body->getName() << " of Link '" << this->link->GetScopedName() << std::endl;
        auto weld = boost::make_shared<OpenSim::WeldConstraint>(
          master->body->getName()+"-w-"+slave->body->getName(),
          *master->body,
          SimTK::Vec3(0), SimTK::Vec3(0),
          *slave->body,
          SimTK::Vec3(0), SimTK::Vec3(0));
        _osimModel.addConstraint(weld.get());
        slave->slave_weld = std::move(weld);
      }
    }
}


//////////////////////////////////////////////////
std::string OpensimLinkPrivate::GetOsimName() const
{
  return const_cast<OpensimLinkPrivate*>(this)->getOsimBody().getName();
}


/////////////////////////////////////////////////
SimTK::MassProperties OpensimLinkPrivate::GetMassProperties() const
{
  if (!this->link->IsStatic())
  {
    InertialPtr inertial = this->link->GetInertial();
    const SimTK::Real mass = inertial->Mass();
    SimTK::Transform X_LI = physics::Pose2Transform(
      inertial->Pose());
    const SimTK::Vec3 &com_L = X_LI.p();  // vector from Lo to com, exp. in L

    if (ignition::math::equal(mass, 0.0))
      return SimTK::MassProperties(mass, com_L, SimTK::UnitInertia(1, 1, 1));

    // Get mass-weighted central inertia, expressed in I frame.
     SimTK::Inertia Ic_I(inertial->IXX(),
                 inertial->IYY(),
                 inertial->IZZ(),
                 inertial->IXY(),
                 inertial->IXZ(),
                 inertial->IYZ());
    // Re-express the central inertia from the I frame to the L frame.
    SimTK::Inertia Ic_L = Ic_I.reexpress(~X_LI.R());  // Ic_L=R_LI*Ic_I*R_IL
    // Shift to L frame origin.
    SimTK::Inertia Io_L = Ic_L.shiftFromMassCenter(-com_L, mass);
    return SimTK::MassProperties(mass, com_L, Io_L);  // convert to unit inertia
  }
  else
  {
    gzerr << "inertial block no specified, using unit mass properties\n";
    return SimTK::MassProperties(1, SimTK::Vec3(0),
      SimTK::UnitInertia(0.1, 0.1, 0.1));
  }
}

/////////////////////////////////////////////////
// When a link is broken into several fragments (master and slaves), they
// share the mass equally. Given the number of fragments, this returns the
// appropriate mass properties to use for each fragment. Per Opensim's
// convention, COM is measured from, and inertia taken about, the link
// origin and both are expressed in the link frame.
SimTK::MassProperties OpensimLinkPrivate::GetEffectiveMassProps(int _numFragments) const
{
    SimTK::MassProperties massProps = this->GetMassProperties();
    GZ_ASSERT(_numFragments > 0, "_numFragments must be at least 1 for the master");
    return SimTK::MassProperties(massProps.getMass()/_numFragments,
                          massProps.getMassCenter(),
                          massProps.getUnitInertia());
}


/////////////////////////////////////////////////
OpensimJointPrivate::OpensimJointPrivate(OpensimJoint* _joint, OpensimPhysics *_engine) :
    engine(_engine),
    joint(_joint),
    outboardLink(nullptr),
    mob_index(MobIndexStateIndicator::MOB_INDEX_UNINITIALIZED),
    isForceAddedToModel {false, false }
{
  this->mustBreakLoopHere = false;
}

/////////////////////////////////////////////////
Mob& OpensimJointPrivate::GetMob()
{
  OpensimLinkPrivate* link_priv = this->outboardLink->GetPriv();
  GZ_ASSERT(link_priv != nullptr, "Joint should be associated with Opensim link");
  auto it = std::find_if(link_priv->mobs.begin(),
                      link_priv->mobs.end(),
                      [&](const Mob &p) -> bool {
                        return p.gzJoint == this->joint;
                      });
  GZ_ASSERT(it != link_priv->mobs.end(), "Joint should be associated with Opensim link");
  GZ_ASSERT(it->gzLink == this->outboardLink, "The gzLink pointer of a Mob must point to the outboard link they are associated with.");
  return *it;
}

/////////////////////////////////////////////////
void OpensimJointPrivate::InitializeJointForces()
{
  OpenSim::Model &osimModel = this->engine->GetPriv()->osimModel;
  OpenSim::Joint *osimJoint = this->GetMob().joint.get();
  GZ_ASSERT(osimJoint != nullptr, "Initialize the outboard link of the joint before calling this function");

  const OpenSim::CoordinateSet& coordinateSet = osimJoint->get_CoordinateSet();

  for (unsigned int idx = 0; idx < this->joint->DOF(); ++idx)
  {
    this->externalCoordinateForce[idx].setCoordinate(&coordinateSet[idx]);
    this->externalCoordinateForce[idx].setName("MDF_"+coordinateSet[idx].getName());
    osimModel.addForce(&this->externalCoordinateForce[idx]);

    {
      auto &force = this->springCoordinateForce[idx];
      force.set_coordinate(coordinateSet[idx].getName());
      force.setName("SPR_"+coordinateSet[idx].getName());
      osimModel.addForce(&force);
    }

    {
      auto &force = this->coordinateLimitForce[idx];
      force.set_coordinate(coordinateSet[idx].getName());
      force.setName("LIM_"+coordinateSet[idx].getName());
      this->SetOpenSimForceParameters(idx);
      osimModel.addForce(&force);
    }

    this->isForceAddedToModel[idx] = true;
  }
}

//////////////////////////////////////////////////
/*
In  the following there is an overview of gazebo setters
and getters for joint limit forces:

SetHighStop  - virtual - implementation in gazebo::physics::Joint calls SetUpperLimit
SetLowStop   - virtual - dito with SetLowerLimit
GetHighStop  - pure virtual
GetLowStop   - pure virtual

not virtual:
GetLowerLimit - set/get internal variables
GetUpperLimit - dito
SetLowerLimit - dito
SetUpperLimit - dito

TODO: Find a way to make GetLower/UpperLimit work after the model
containing the joint has been initialized. Perhaps do a check every
time step - update force if values changed.
 */
void OpensimJointPrivate::SetOpenSimForceParameters(unsigned int _index)
{
  OpenSim::Joint *osimJoint = this->GetMob().joint.get();
  bool is_rotational = osimJoint->getCoordinateSet()[_index].getMotionType()==OpenSim::Coordinate::Rotational;
  auto &force = this->coordinateLimitForce[_index];
  // Rotational limits are specified in degrees. Hence conversion is needed.
  // In the SDF they are given in radians (or meters).
  auto l0_boxed = this->joint->LowerLimit(_index);
  auto l1_boxed = this->joint->UpperLimit(_index);
  // The Degree member performs multiplication by 180./pi. The Radian
  // member just returns the value, I believe.
  double l0 = is_rotational ? ignition::math::Angle(l0_boxed).Degree() : ignition::math::Angle(l0_boxed).Radian();
  double l1 = is_rotational ? ignition::math::Angle(l1_boxed).Degree() : ignition::math::Angle(l1_boxed).Radian();
  double conversion_factor = is_rotational ? M_PI / 180.0 : 1.0;
  if (l1 < l0)
    std::swap(l1, l0);
  force.setUpperLimit(l1);
  force.setLowerLimit(l0);
  double kp = conversion_factor * this->engine->stopStiffnessMultiplier * this->joint->GetStopStiffness(_index);
  double kd = conversion_factor * this->engine->stopDissipationMultiplier * this->joint->GetStopDissipation(_index);
  double kw = this->engine->stopWidthFraction;
  force.setUpperStiffness(kp);
  force.setLowerStiffness(kp);
  force.setDamping(kd);
  // Simbody does not like this being zero, so take at least 1.e-12
  force.setTransition(std::max(1.e-12, kw * (l1 - l0)));
}


/////////////////////////////////////////////////
/// Free standing utility function
/////////////////////////////////////////////////

namespace opensim_internal
{
/////////////////////////////////////////////////
std::string ShapeTypeStr(unsigned int t)
{
#define SHAPE_TYPE_STR_CASE(enum_symbol) \
  case physics::Entity::enum_symbol: { return #enum_symbol; } break;

  switch (t & (~physics::Entity::SHAPE))
  {
    SHAPE_TYPE_STR_CASE(PLANE_SHAPE);
    SHAPE_TYPE_STR_CASE(SPHERE_SHAPE);
    SHAPE_TYPE_STR_CASE(MESH_SHAPE);
    SHAPE_TYPE_STR_CASE(CYLINDER_SHAPE);
    SHAPE_TYPE_STR_CASE(BOX_SHAPE);
    SHAPE_TYPE_STR_CASE(HEIGHTMAP_SHAPE);
    SHAPE_TYPE_STR_CASE(MAP_SHAPE);
    SHAPE_TYPE_STR_CASE(MULTIRAY_SHAPE);
    SHAPE_TYPE_STR_CASE(RAY_SHAPE);
    SHAPE_TYPE_STR_CASE(POLYLINE_SHAPE);
    default:
      gzthrow ("unsupported type");
  }
}

/////////////////////////////////////////////////
SimTK::Quaternion QuadToQuad(const ignition::math::Quaterniond &_q)
{
  return SimTK::Quaternion(_q.W(), _q.X(), _q.Y(), _q.Z());
}

/////////////////////////////////////////////////
ignition::math::Quaterniond QuadToQuad(const SimTK::Quaternion &_q)
{
  return ignition::math::Quaterniond(_q[0], _q[1], _q[2], _q[3]);
}

/////////////////////////////////////////////////
SimTK::Vec3 Vector3ToVec3(const ignition::math::Vector3d &_v)
{
  return SimTK::Vec3(_v.X(), _v.Y(), _v.Z());
}

/////////////////////////////////////////////////
ignition::math::Vector3d Vec3ToVector3(const SimTK::Vec3 &_v)
{
  return ignition::math::Vector3d(_v[0], _v[1], _v[2]);
}

/////////////////////////////////////////////////
SimTK::Transform Pose2Transform(const ignition::math::Pose3d &_pose)
{
  SimTK::Quaternion q(_pose.Rot().W(), _pose.Rot().X(), _pose.Rot().Y(),
                   _pose.Rot().Z());
  SimTK::Vec3 v(_pose.Pos().X(), _pose.Pos().Y(), _pose.Pos().Z());
  SimTK::Transform frame(SimTK::Rotation(q), v);
  return frame;
}

/////////////////////////////////////////////////
ignition::math::Pose3d Transform2Pose(const SimTK::Transform &_xAB)
{
  SimTK::Quaternion q(_xAB.R());
  const SimTK::Vec4 &qv = q.asVec4();
  return ignition::math::Pose3d(ignition::math::Vector3d(_xAB.p()[0], _xAB.p()[1], _xAB.p()[2]),
    ignition::math::Quaterniond(qv[0], qv[1], qv[2], qv[3]));
}

/////////////////////////////////////////////////
SimTK::Transform GetPose(sdf::ElementPtr _element)
{
  const ignition::math::Pose3d pose = _element->Get<ignition::math::Pose3d>("pose");
  return Pose2Transform(pose);
}
} // namespace opensim_internal

}} // namespace physics // namespace gazebo
