/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Exception.hh"
#include "gazebo/common/Console.hh"

#include "gazebo/physics/Model.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimUniversalJoint.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimUniversalJoint::OpensimUniversalJoint(BasePtr _parent, OpensimPhysics &_engine)
    : UniversalJoint<OpensimJoint>(_parent)
{
}

//////////////////////////////////////////////////
OpensimUniversalJoint::~OpensimUniversalJoint()
{
}

//////////////////////////////////////////////////
void OpensimUniversalJoint::Load(sdf::ElementPtr _sdf)
{
  UniversalJoint<OpensimJoint>::Load(_sdf);
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimUniversalJoint::Anchor(unsigned int /*_index*/) const
{
  return this->anchorPos;
}

//////////////////////////////////////////////////
// ignition::math::Vector3d OpensimUniversalJoint::GetAxis(unsigned int /*_index*/) const
// {
//   return ignition::math::Vector3d();
// }

//////////////////////////////////////////////////
void OpensimUniversalJoint::SetAxis(unsigned int /*_index*/,
                                    const ignition::math::Vector3d & /*_axis*/)
{
  /// Universal Joint are built in OpensimPhysics.cc, so this init block
  /// actually does nothing.
  gzdbg << "SetAxis: setting axis is not yet implemented. The axes are set "
        << "during joint construction in OpensimPhyiscs.cc for now.\n";
}

//////////////////////////////////////////////////
double OpensimUniversalJoint::GetVelocity(unsigned int _index) const
{
  return 0;
  //   if (_index < this->GetAngleCount())
  //   {
  //     if (this->physicsInitialized &&
  //         this->opensimPhysics->opensimPhysicsInitialized)
  //     {
  //       return this->mobod.getOneU(
  //         this->opensimPhysics->integ->getState(),
  //         SimTK::MobilizerUIndex(_index));
  //     }
  //     else
  //     {
  //       gzdbg << "GetVelocity() opensim not yet initialized, "
  //             << "initial velocity should be zero until restart from "
  //             << "state has been implemented.\n";
  //       return 0.0;
  //     }
  //   }
  //   else
  //   {
  //     gzerr << "Invalid index for joint, returning NaN\n";
  //     return SimTK::NaN;
  //   }
}

//////////////////////////////////////////////////
void OpensimUniversalJoint::SetVelocity(unsigned int _index,
                                        double _rate)
{
  //   if (_index < this->GetAngleCount())
  //   {
  //     this->mobod.setOneU(
  //       this->opensimPhysics->integ->updAdvancedState(),
  //       SimTK::MobilizerUIndex(_index), _rate);
  //     this->opensimPhysics->system.realize(
  //       this->opensimPhysics->integ->getAdvancedState(), SimTK::Stage::Velocity);
  //   }
  //   else
  //   {
  //     gzerr << "SetVelocity _index too large.\n";
  //   }
}

//////////////////////////////////////////////////
void OpensimUniversalJoint::SetForceImpl(unsigned int _index,
                                         double _torque)
{
  //   if (_index < this->GetAngleCount() && this->physicsInitialized)
  //   {
  //     this->opensimPhysics->discreteForces.setOneMobilityForce(
  //       this->opensimPhysics->integ->updAdvancedState(),
  //       this->mobod, SimTK::MobilizerUIndex(_index), _torque);
  //   }
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimUniversalJoint::GlobalAxis(
    unsigned int _index) const
{
  //   if (this->opensimPhysics &&
  //       this->opensimPhysics->opensimPhysicsStepped &&
  //       _index < this->GetAngleCount())
  //   {
  //     if (!this->mobod.isEmptyHandle())
  //     {
  //       if (_index == UniversalJoint::AXIS_PARENT)
  //       {
  //         // express X-axis of X_IF in world frame
  //         const SimTK::Transform &X_IF = this->mobod.getInboardFrame(
  //           this->opensimPhysics->integ->getState());
  //
  //         SimTK::Vec3 x_W(
  //           this->mobod.getParentMobilizedBody().expressVectorInGroundFrame(
  //           this->opensimPhysics->integ->getState(), X_IF.x()));
  //
  //         return Vec3ToVector3(x_W);
  //       }
  //       else if (_index == UniversalJoint::AXIS_CHILD)
  //       {
  //         // express Y-axis of X_OM in world frame
  //         const SimTK::Transform &X_OM = this->mobod.getOutboardFrame(
  //           this->opensimPhysics->integ->getState());
  //
  //         SimTK::Vec3 y_W(
  //           this->mobod.expressVectorInGroundFrame(
  //           this->opensimPhysics->integ->getState(), X_OM.y()));
  //
  //         return Vec3ToVector3(y_W);
  //       }
  //       else
  //       {
  //         gzerr << "GetGlobalAxis: internal error, GetAngleCount < 0.\n";
  //         return ignition::math::Vector3d(SimTK::NaN, SimTK::NaN, SimTK::NaN);
  //       }
  //     }
  //     else
  //     {
  //       gzerr << "Joint mobod not initialized correctly.  Returning"
  //             << " initial axis vector in world frame (not valid if"
  //             << " joint frame has moved). Please file"
  //             << " a report on issue tracker.\n";
  //       return this->GetAxisFrame(_index).RotateVector(
  //         this->GetLocalAxis(_index));
  //     }
  //   }
  //   else
  //   {
  //     if (_index >= this->GetAngleCount())
  //     {
  //       gzerr << "index out of bound\n";
  //       return ignition::math::Vector3d(SimTK::NaN, SimTK::NaN, SimTK::NaN);
  //     }
  //     else
  //     {
  //       gzdbg << "GetGlobalAxis() sibmody physics engine not yet initialized, "
  //             << "use local axis and initial pose to compute "
  //             << "global axis.\n";
  //       // if local axis specified in model frame (to be changed)
  //       // switch to code below if issue #494 is to be addressed
  //       return this->GetAxisFrame(_index).RotateVector(
  //         this->GetLocalAxis(_index));
  //     }
  //   }
  return ignition::math::Vector3d();
}

//////////////////////////////////////////////////
double OpensimUniversalJoint::PositionImpl(unsigned int _index) const
{
  //   if (_index < this->GetAngleCount())
  //   {
  //     if (this->physicsInitialized &&
  //         this->opensimPhysics->opensimPhysicsInitialized)
  //     {
  //       if (!this->mobod.isEmptyHandle())
  //       {
  //         return math::Angle(this->mobod.getOneQ(
  //           this->opensimPhysics->integ->getState(), _index));
  //       }
  //       else
  //       {
  //         gzerr << "Joint mobod not initialized correctly.  Please file"
  //               << " a report on issue tracker.\n";
  //         return math::Angle(0.0);
  //       }
  //     }
  //     else
  //     {
  //       gzdbg << "GetAngleImpl(): opensim not yet initialized, "
  //             << "initial angle should be zero until <initial_angle> "
  //             << "is implemented.\n";
  //       return math::Angle(0.0);
  //     }
  //   }
  //   else
  //   {
  //     gzerr << "index out of bound\n";
  //     return math::Angle(SimTK::NaN);
  //   }
  return ignition::math::NAN_D;
}
