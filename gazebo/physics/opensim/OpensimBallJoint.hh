/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIMBALLJOINT_HH_
#define _OPENSIMBALLJOINT_HH_

#include "gazebo/physics/BallJoint.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief OpensimBallJoint class models a ball joint in Opensim.
class GZ_PHYSICS_VISIBLE OpensimBallJoint : public BallJoint<OpensimJoint>
{
  /// \brief Opensim Ball Joint Constructor
public:
  OpensimBallJoint(BasePtr _parent, OpensimPhysics &_engine);

  /// \brief Destructor
public:
  virtual ~OpensimBallJoint();

  // Documentation inherited.
public:
  virtual void Load(sdf::ElementPtr _sdf);

  // Documentation inherited.
public:
  ignition::math::Vector3d Anchor(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d GetAxis(unsigned int /*_index*/) const
  {
    return ignition::math::Vector3d();
  }

  // Documentation inherited.
public:
  virtual void SetVelocity(unsigned int _index, double _angle);

  // Documentation inherited.
public:
  virtual double GetVelocity(unsigned int _index) const;

public:
  virtual double GetAcceleration(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual ignition::math::Angle GetAngleImpl(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d GetGlobalAxis(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual void SetAxis(unsigned int _index,
                       const ignition::math::Vector3d &_axis);

  // Documentation inherited.
public:
  virtual double UpperLimit(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual double LowerLimit(unsigned int _index) const;

  // Documentation inherited.
public:
  virtual bool SetHighStop(unsigned int _index,
                           const ignition::math::Angle &_angle);

  // Documentation inherited.
public:
  virtual bool SetLowStop(unsigned int _index,
                          const ignition::math::Angle &_angle);

  // Documentation inherited.
protected:
  virtual void SetForceImpl(unsigned int _index, double _torque);
};
/// \}
}
}
#endif
