/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_PHYSICS_HH
#define _OPENSIM_PHYSICS_HH
#include <string>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>

#include "gazebo/physics/PhysicsEngine.hh"
#include "gazebo/physics/Collision.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/Shape.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/opensim_inc.h"
#include "gazebo/util/system.hh"

// Seconds to wait for gzbridge's connection
static const double WAIT_GZBRIDGE_SECONDS = 5.0;

namespace gazebo
{
  namespace physics
  {
    class RemovingModels;

    // Private implementation
    class OpensimPhysicsPrivate;

    /// \ingroup gazebo_physics
    /// \addtogroup gazebo_physics_opensim Opensim Physics
    /// \{

    /// \brief Opensim physics engine
    class GZ_PHYSICS_VISIBLE OpensimPhysics : public PhysicsEngine
    {
      

      /* PhysicsEngine function overrrides
      -------------------------------------*/
      OpensimPhysics(const OpensimPhysics &) = delete;
      OpensimPhysics& operator=(const OpensimPhysics &) = delete;

      /// \brief Constructor
      public: OpensimPhysics(WorldPtr _world);

      /// \brief Destructor
      public: virtual ~OpensimPhysics() override;

      // Documentation inherited
      public: virtual void Load(sdf::ElementPtr _sdf) override;

      // Documentation inherited
      public: virtual void Init() override;

      // Documentation inherited
      public: virtual void Reset() override;

      // Documentation inherited
      public: virtual void InitForThread() override;

      // Documentation inherited
      public: virtual void UpdateCollision() override;

      // Documentation inherited
      public: virtual void UpdatePhysics() override;

      // Documentation inherited
      public: virtual void Fini() override;

      // Documentation inherited
      public: virtual std::string GetType() const override;

      // Documentation inherited
      public: virtual LinkPtr CreateLink(ModelPtr _parent) override;

      // Documentation inherited
      public: virtual CollisionPtr CreateCollision(const std::string &_type,
                                                   LinkPtr _body) override;

      // Documentation inherited
      public: virtual JointPtr CreateJoint(const std::string &_type,
                                           ModelPtr _parent) override;

      // Documentation inherited
      public: virtual ShapePtr CreateShape(const std::string &_shapeType,
                                           CollisionPtr _collision) override;

      // Documentation inherited
      public: virtual void SetGravity(const ignition::math::Vector3d &_gravity) override;

      // Documentation inherited
      public: virtual void DebugPrint() const override;

      // Documentation inherited
      public: virtual void SetSeed(uint32_t _seed) override;

      // Documentation inherited
      public: virtual ModelPtr CreateModel(BasePtr _parent) override;

      // Documentation inherited
      protected: virtual void OnRequest(ConstRequestPtr &_msg) override;

      // Documentation inherited
      protected: virtual void OnPhysicsMsg(ConstPhysicsPtr &_msg) override;

      // Documentation inherited
      public: virtual boost::any GetParam(const std::string &_key) const override;

      // Documentation inherited
      public: virtual bool GetParam(const std::string &_key,
                  boost::any &_value) const override;

      // Documentation inherited
      public: virtual bool SetParam(const std::string &_key,
                  const boost::any &_value) override;

                  /// \brief Removes association of opensim elements (body, joint, forces, etc.) from the opensim Model instance.
      public: void RemoveModelHierarchyFromOpensimSystem(OpensimModel &_model);

      /// \brief Removes collision shapes of link from the opensim model.
      private: void RemoveCollisionsFromOpensimSystem(const Collision_V &collisions);

      /// \brief Internal! Used by OpensimModel to add itself to the system.
      /// \param[in] _model Pointer to the model.
      public: void AddModelHierarchyToOpensimSystem(OpensimModel *_model);

      /// \brief Add a static Model to opensim system, and reinitialize state
      /// \param[in] _model the incoming static Gazebo physics::Model.
      private: void AddStaticModelToOpensimSystem(OpensimModel &_model);

      /// \brief Read from MultibodydGraphMaker and construct a physics::Model.
      /// \param[in] _mbgraph Contain MultibodyGraphMaker object.
      private: void AddDynamicModelsToOpensimSystem(
        const SimTK::MultibodyGraphMaker &_mbgraph);

      private: void AddLinkCollisions(
        const OpensimLink &_link,
        OpenSim::Body &_body,
        SimTK::ContactCliqueId _modelClique);

      private: void SystemUpdateAfterTopologyChange();

      /// \brief Query how many collision shapes have been created by the given _type; returns 0 if _type is invalid.
      //public: unsigned int GetCreatedShapesCount(const std::string& _type) const;

      /// \brief Set how many collision shapes have been created for the given _type.
      //public: void SetCreatedShapesCount(const std::string& _type, unsigned int _count);

#ifdef OPENSIM_USE_DEBUG_DRAWER
      /// \brief Get DebugDrawer instance
      public: gazebo::physics::DebugDrawer* GetDebugDrawer();
#endif

      /// \brief Get the world
      public: gazebo::physics::WorldPtr World() { return this->world; }

      /// \brief Publisher for debug drawing messages
      //public: transport::PublisherPtr getDebugDrawPub() { return debugDrawPub; }

      /// \brief True if Init() has been called.
      ///
      /// Used to defer initial system construction until all models are loaded.
      public: bool opensimPhysicsFirstStart;

      /// \brief True if the physics system is in a consistent state where accessors work and the physics loop can be run.
      public: bool opensimPhysicsInitialized;

      //public: bool opensimPhysicsStepped;
      //private: common::Time lastUpdateTime;

      //private: double stepTimeDouble;

      /// \brief Multiplier for the joint limit force spring constant.
      ///
      /// The spring constant actually taken is joint->GetStopStiffness()
      /// times stopStiffnessMultiplier. The reasoning behind this is the
      /// extreme mangnitude of 1e8 of the default value. This is causing
      ///  problems with simbody. To make things work out of the box, the
      ///  correction factors stopStiffnessMultiplier and
      ///  stopDissipationMultiplier are introduced.
      public: double stopStiffnessMultiplier;

      /// \brief See stopStiffnessMultiplier
      public: double stopDissipationMultiplier;

      /// \brief Fractional width over which the joint limit spring constant increases.
      ///
      /// Relative allowed joint angel range.
      public: double stopWidthFraction;

      public: OpensimCollision::SurfaceParameters globalOverrideSurfaceParams;

//       // TODO: Use these parameters!
//       /// \brief contact material stiffness.  See sdf description for details.
//       private: double contactMaterialStiffness;
//
//       /// \brief contact material dissipation.  See sdf description for details.
//       private: double contactMaterialDissipation;
//
//       /// \brief contact material static friction.
//       /// See sdf description for details.
//       private: double contactMaterialStaticFriction;
//
//       /// \brief contact material dynamic friction.
//       /// See sdf description for details.
//       private: double contactMaterialDynamicFriction;
//
//       /// \brief contact material viscous friction.
//       /// See sdf description for details.
//       private: double contactMaterialViscousFriction;

      /// \brief contact material plastic coefficient of restitution.
      /// See sdf description for details.
      //private: double contactMaterialPlasticCoefRestitution;

      /// \brief contact material plastic impact velocity.
      /// See sdf description for details.
      //private: double contactMaterialPlasticImpactVelocity;

      /// \brief contact impact capture velocity.
      /// See sdf description for details.
      //private: double contactImpactCaptureVelocity;

      /// \brief contact stiction transition velocity
      /// See sdf description for details.
      //private: double contactStictionTransitionVelocity;

      /// \brief Node for communication.
      private: transport::NodePtr node;

      /// \brief Info publisher on muscles.
      private: transport::PublisherPtr muscleInfoPublisher;

      /// \brief Publisher on ~/error.
      private: transport::PublisherPtr errorPublisher;
      
      /// \brief If true, wait for a connection before publishing on ~/error.
      private: bool waitForConnectionOnError;

      public:  OpensimPhysicsPrivate* GetPriv() { return dataPtr.get(); }
      public: const OpensimPhysicsPrivate* GetPriv() const { return const_cast<OpensimPhysics*>(this)->GetPriv(); }

      private: boost::shared_ptr<OpensimPhysicsPrivate> dataPtr;

      private: friend OpensimPhysicsPrivate;
      private: friend RemovingModels;
    };
  /// \}
  }
}
#endif
