#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimCylinderShape.hh"
#include "gazebo/physics/opensim/OpensimMeshShape.hh"
#include "gazebo/common/MeshManager.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Mesh.hh"
#include "gazebo/physics/World.hh"

#include "gazebo/physics/opensim/opensim_inc.h"
#include "OpensimPhysicsPrivate.hh"


using namespace gazebo;
using namespace physics;

void OpensimCylinderShape::SetSize(double _radius, double _length)
{
  if (_radius < 0)
  {
    gzerr << "Cylinder shape does not support negative radius\n";
    return;
  }
  if (_length < 0)
  {
    gzerr << "Cylinder shape does not support negative length\n";
    return;
  }
  if (ignition::math::equal(_radius, 0.0))
  {
    // Warn user, but still create shape with very small value
    // otherwise later resize operations using setLocalScaling
    // will not be possible
    gzwarn << "Setting cylinder shape's radius to zero \n";
    _radius = 1e-4;
  }
  if (ignition::math::equal(_length, 0.0))
  {
    gzwarn << "Setting cylinder shape's length to zero \n";
    _length = 1e-4;
  }
  CylinderShape::SetSize(_radius, _length);
}


//////////////////////////////////////////////////
void OpensimCylinderShape::Init()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  CylinderShape::Init();
}

//////////////////////////////////////////////////
void OpensimCylinderShape::AddToBody(OpensimPhysicsPrivate *_engine_priv, OpenSim::Body &_body)
{
  OpensimCollisionPtr collision = boost::dynamic_pointer_cast<OpensimCollision>(this->collisionParent);

  Transform X_LC =
      Pose2Transform(
          collision->RelativePose());

  // convert to position/euler representation
  SimTK::Vec3 pos = X_LC.p();
  SimTK::Vec3 euler = X_LC.R().convertRotationToBodyFixedXYZ();

  const char* MESH_NAME = "OPENSIM_CYLINDER_MESH";
  gazebo::common::MeshManager::Instance()->CreateCylinder(
    MESH_NAME, 1., 1.,
    2, 16, 1, true);
  const common::Mesh* mesh = gazebo::common::MeshManager::Instance()->GetMesh(MESH_NAME);

  // The sdf format does not specify the scale tag for cylinders. It is therefore
  // sufficient to consider the following for correct sizing.
  auto size = ignition::math::Vector3d(this->GetRadius(), this->GetRadius(), this->GetLength());

  SimTK::PolygonalMesh simtk_mesh;
  ConvertToSimTK(simtk_mesh, *mesh, size);

  std::string osim_name = _engine_priv->CreateUniqueOsimNameFor(collision.get());
  this->osimGeometry = boost::make_shared<OpenSim::ContactMesh>(
    simtk_mesh, pos, euler, _body, osim_name);

  _engine_priv->osimModel.addContactGeometry(this->osimGeometry.get());

  OPENSIM_PHYSICS_DEBUG1(gzdbg << "Created new OpenSim ContactMesh for "
                              << collision->GetName() << " (" << osim_name << ") "
                              << ", position = " << pos
                              << ", orientation = " << euler
                              << std::endl;)
}
