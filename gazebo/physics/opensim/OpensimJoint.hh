/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_JOINT_HH_
#define _OPENSIM_JOINT_HH_

#include <boost/any.hpp>
#include <string>

#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief Base class for all joints
class GZ_PHYSICS_VISIBLE OpensimJoint : public Joint
{
  friend class OpensimPhysics;

  /// \brief Constructor
public:
  OpensimJoint(BasePtr _parent);

  /// \brief Destructor
public:
  virtual ~OpensimJoint() override;

  // Documentation inherited.
public:
  virtual void Load(sdf::ElementPtr _sdf) override;

  // Documentation inherited.
public:
  virtual void Reset() override;

  // Documentation inherited.
public:
  virtual void Fini() override;

  // Documentation inherited.
public:
  virtual LinkPtr GetJointLink(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual bool AreConnected(LinkPtr _one, LinkPtr _two) const override;

  // Documentation inherited.
public:
  virtual void Detach() override;

  // Documentation inherited.
public:
  virtual void SetAnchor(unsigned int _index,
                         const ignition::math::Vector3d &_anchor) override;

  // Documentation inherited.
public:
  virtual void SetDamping(unsigned int _index,
                          const double _damping) override;

  // Documentation inherited.
public:
  virtual void SetStiffness(unsigned int _index,
                            const double _stiffness) override;

  // Documentation inherited.
public:
  virtual void SetStiffnessDamping(unsigned int _index,
                                   double _stiffness, double _damping, double _reference = 0) override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d Anchor(const unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d LinkForce(const unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d LinkTorque(const unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual bool SetParam(const std::string &_key,
                        unsigned int _index,
                        const boost::any &_value) override;

  //       // Save current Opensim State
  //       public: virtual void SaveOpensimState(const SimTK::State &_state);
  //
  //       // Restore saved Opensim State
  //       public: virtual void RestoreOpensimState(SimTK::State &_state);

  // Documentation inherited.
public:
  virtual void SetForce(unsigned int _index, double _force) override;

  // Documentation inherited.
public:
  virtual double GetForce(unsigned int _index) override;

  // Documentation inherited.
public:
  virtual void SetAxis(const unsigned int _index,
                       const ignition::math::Vector3d &_axis) override;

  // Documentation inherited.
public:
  virtual JointWrench GetForceTorque(unsigned int _index) override;

  /// \brief Set the force applied to this physics::Joint.
  /// Note that the unit of force should be consistent with the rest
  /// of the simulation scales.
  /// Force is additive (multiple calls
  /// to SetForceImpl to the same joint in the same time
  /// step will accumulate forces on that Joint).
  /// \param[in] _index Index of the axis.
  /// \param[in] _force Force value.
  /// internal force, e.g. damping forces.  This way, Joint::appliedForce
  /// keep track of external forces only.
protected:
  virtual void SetForceImpl(unsigned int _index,
                            double _force);

  // Documentation inherited.
public:
  virtual void CacheForceTorque() override;

  // Documentation inherited.
public:
  virtual void SetUpperLimit(const unsigned int _index,
                           const double _limit) override;

  // Documentation inherited.
public:
  virtual void SetLowerLimit(const unsigned int _index,
                          const double _limit) override;

  // Documentation inherited.
public:
  virtual double UpperLimit(const unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual double LowerLimit(const unsigned int _index) const override;

  /// \brief The initial(?) pose of this joint relative to the child frame.
public:
  const ignition::math::Pose3d &GetChildAnchorPose() const { return this->anchorPose; }

  /// \brief The initial(?) pose of this joint relative to the parent frame.
public:
  const ignition::math::Pose3d &GetParentAnchorPose() const { return this->parentAnchorPose; }

  // Documentation inherited.
public:
  virtual void SetVelocity(unsigned int _index, double _rate) override;

  // Documentation inherited.
public:
  virtual double GetVelocity(unsigned int _index) const override;

public:
  // Documentation inherited.
  virtual double GetAcceleration(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d GlobalAxis(unsigned int _index) const override;

  // Documentation inherited.
protected:
  virtual double PositionImpl(const unsigned int _index) const override;

protected:
  virtual bool CheckValidAxisAndDebugOut(unsigned int _axis, const char *_where) const;

  ///////////////////////////////////////////////////////////////////
  ///  Private members, used by opensim physics implementation!
  ///////////////////////////////////////////////////////////////////

public:
  friend class OpensimJointPrivate;

public:
  OpensimJointPrivate *GetPriv() { return dataPtr.get(); }
public:
  const OpensimJointPrivate *GetPriv() const { return const_cast<OpensimJoint *>(this)->GetPriv(); }

  /// \brief: Private implementation. Pointer initialized in derived classes.
protected:
  boost::shared_ptr<OpensimJointPrivate> dataPtr;
};
/// \}
}
}
#endif
