/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/opensim/OpensimMuscle.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

#include "gazebo/common/Assert.hh"
#include <gazebo/common/Console.hh>

#include <algorithm>

namespace OpenSim
{

//////////////////////////////////////////////////
/// @brief Forward control value to the simbody system.
//////////////////////////////////////////////////
class OSIMSIMULATION_API ExternalActuatorController : public Controller
{
    OpenSim_DECLARE_CONCRETE_OBJECT(ExternalActuatorController, Controller)
  public:
        /// @brief  Constructor. Takes the controled actuator by pointer.
        ExternalActuatorController(Actuator *_actuator);

        /**
         * @brief Add the control value for the controled actuator into the controls array.
         *
         * @param s             system state
         * @param controls      model controls
         */
        void computeControls(const SimTK::State& s,
                             SimTK::Vector& controls) const override;

        /// @brief Assign a control value
        void setControlValue(double value);
        double getControlValue() const;

protected:
        /// @brief Ugly opensim hacks
        void connectToModel(Model& model) override;

        /// @brief Buffer for control value.
        SimTK::Vector controlValues;
};



//////////////////////////////////////////////////
ExternalActuatorController::ExternalActuatorController(Actuator *_actuator) :
        Controller(),
        controlValues(1, 0.0)
{
  // With this implementation we most likely avoid referencing muscles
  // by name which could cause problems when names are not unique.

  GZ_ASSERT(_actuator != nullptr, "Actuator argument must not be NULL!");
  OpenSim::Set<OpenSim::Actuator> a;
  a.setMemoryOwner(false);
  a.adoptAndAppend(_actuator);
  this->setActuators(a);
}

//////////////////////////////////////////////////
void ExternalActuatorController::computeControls(const SimTK::State& s, SimTK::Vector& controls) const
{
  auto &acts = this->getActuatorSet();
  GZ_ASSERT(acts.getSize() == 1, "There should be exactly one controled actuator!");
  //gzdbg << "muscle " << acts[0].getName() << " (" << (&acts[0]) << ") act = " << this->controlValues[0] << std::endl;
  acts[0].addInControls(this->controlValues, controls);
}

//////////////////////////////////////////////////
void ExternalActuatorController::setControlValue(double value)
{
  this->controlValues[0] = value;
}


double ExternalActuatorController::getControlValue() const
{
  return this->controlValues[0];
}


//////////////////////////////////////////////////
void ExternalActuatorController::connectToModel(Model& model)
{
  // DO NOT call the superclass but the next higher super class
  // This is because Controller::connectToModel's only job is
  // to do it's best to destroy our array set by this->setActuators().
  ModelComponent::connectToModel(model);
  // Also clear the control values, e.g. for when the world is reset.
  std::fill(this->controlValues.begin(), this->controlValues.end(), 0.);
}


} // namespace OpenSim


namespace gazebo
{
namespace physics
{

//////////////////////////////////////////////////
OpensimPathActuator::OpensimPathActuator(OpenSim::PathActuator *_osimMuscle, const std::string &_name, OpensimPhysics &_engine) :
    osimActuator(_osimMuscle), name(_name), engine_priv(_engine.GetPriv())
{
  this->osimController = boost::make_shared<OpenSim::ExternalActuatorController>(this->osimActuator);
  this->engine_priv->osimModel.addController(this->osimController.get());
}

//////////////////////////////////////////////////
OpensimPathActuator::~OpensimPathActuator()
{

}

//////////////////////////////////////////////////
void OpensimPathActuator::SetActivationValue(double value)
{
  this->osimController->setControlValue(value);
  gzdbg << "SetActivation Muscle = " << GetName() << ", activation = " << value << std::endl;
}

//////////////////////////////////////////////////
double OpensimPathActuator::GetActivationValue() const
{
  return this->osimController->getControlValue();
}

//////////////////////////////////////////////////
void OpensimPathActuator::GetCurrentWorldPath(std::vector<ignition::math::Vector3d> &_positions) const
{
  const SimTK::State &state = engine_priv->getWorkingStateRealizedToStage(SimTK::Stage::Position);

  const OpenSim::Array<OpenSim::PathPoint*> &current_path = this->osimActuator->getGeometryPath().getCurrentPath(state);

  std::size_t sz = current_path.getSize();

  _positions.clear();
  _positions.reserve(sz);
  for (std::size_t i=0; i<sz; ++i)
  {
    SimTK::Vec3 loc = current_path[i]->getLocation(); // local location
    const OpenSim::Body& body = current_path[i]->getBody();
    const SimTK::Transform bodyTrafo = engine_priv->osimModel.getSimbodyEngine().getTransform(state, body);
    loc = bodyTrafo * loc;
    _positions.push_back(Vec3ToVector3(loc));
  }
}

//////////////////////////////////////////////////
int OpensimPathActuator::GetPathNodeCount() const
{
  const OpenSim::PathPointSet& pointset = this->osimActuator->getGeometryPath().getPathPointSet();
  return pointset.getSize();
}

/////////////////////////////////////////////////
void OpensimPathActuator::RemoveFrom(OpenSim::Model &osimModel)
{
  remove<OpenSim::Controller>(osimModel.updControllerSet(), osimController.get());
}

/////////////////////////////////////////////////
double OpensimPathActuator::GetOptimalFiberLength() const
{
  if (auto *muscle = dynamic_cast<const OpenSim::Muscle*>(this->osimActuator))
  {
    return muscle->getOptimalFiberLength();
  }
  else return 0;
}

/////////////////////////////////////////////////
double OpensimPathActuator::GetTendonSlackLength() const
{
  if (auto *muscle = dynamic_cast<const OpenSim::Muscle*>(this->osimActuator))
  {
    return muscle->getTendonSlackLength();
  }
  else return 0;
}

/////////////////////////////////////////////////
double OpensimPathActuator::GetForce() const
{
  const SimTK::State& state = engine_priv->getWorkingStateRealizedToStage(SimTK::Stage::Dynamics);
  return this->osimActuator->getForce(state);
}

/////////////////////////////////////////////////
double OpensimPathActuator::GetLength() const
{
  const SimTK::State& state = engine_priv->getWorkingStateRealizedToStage(SimTK::Stage::Position);
  return this->osimActuator->getLength(state);
}

/////////////////////////////////////////////////
double OpensimPathActuator::GetLengtheningSpeed() const
{
  const SimTK::State& state = engine_priv->getWorkingStateRealizedToStage(SimTK::Stage::Velocity);
  return this->osimActuator->getLengtheningSpeed(state);
}

/////////////////////////////////////////////////
std::string OpensimPathActuator::GetName() const
{
  return name;
}

/////////////////////////////////////////////////
bool OpensimPathActuator::IsMuscle() const
{
  return dynamic_cast<const OpenSim::Muscle*>(this->osimActuator);
}


} // namespace physics
} // namespace gazebo
