/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef OPENSIMCONTACTFORCEMANAGER_H
#define OPENSIMCONTACTFORCEMANAGER_H

#include "gazebo/util/system.hh"
#include "opensim_inc.h"
#include "OpensimPhysics.hh"

#include <OpenSim/OpenSim.h>


namespace gazebo
{
namespace physics
{
class GZ_PHYSICS_VISIBLE OpensimContactForceManager
{
public:
  using ContactData = SimTK::GeneralContactForceFeedback::ContactData;

public:
  OpensimContactForceManager(OpensimPhysicsPrivate *);

public:
  ~OpensimContactForceManager();

public:
  /// @bried Add a number of new collisions.
  void AddToContactForceSet(const Collision_V &collisions);

public:
  /// @brief Remove a number of collision shapes from collision handling.
  void RemoveFromContactForceSet(const Collision_V &collisions);

public:
  /// @brief Collect contact point after physics step so that they can be accessed by Begin/EndContactData.
  void UpdateContactData();

public:
  /// @brief iterators over last contact points.
  ContactData::const_iterator BeginContactData() const { return contact_data.begin(); }
  ContactData::const_iterator EndContactData() const { return contact_data.end(); }

private:
  /// @brief Collision shapes handled.
  Collision_V all_collisions;

  /// @brief One force instance per collision pair.
  struct Force
  {
    // EF and HC obtain contact points in the same way(?). They define a set of
    // bodies admissible for mutual collision. At the moment the set is filled
    // with two bodies because omission of intra-model collision leave little
    // choice. The EF and HC instance is served with a list of contact points
    // corresponding to this set. The function calls for that mean some overhead
    // but the determination of contact points itself is probably quite efficient.
    OpenSim::ElasticFoundationForce ef;
    OpenSim::HuntCrossleyForce hc;
    /// @brief Keep collision pair handled by this force. Needed to remove the force when collision go away.
    const Collision* collision_pair[2];

    Force(OpensimPhysicsPrivate *engine_priv, const OpensimCollision &collision_1, const OpensimCollision &collision_2);
    void RemoveForces(OpensimPhysicsPrivate *engine_priv);
    void InsertContactDataIn(ContactData &contact_data);
  };
  std::vector<boost::shared_ptr<Force>> forces;
  OpensimPhysicsPrivate *engine_priv;
  /// @brief Data collected from EF and HC force instances.
  ContactData contact_data;
};
}
}
#endif // OPENSIMCONTACTFORCEMANAGER_H
