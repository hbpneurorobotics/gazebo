/*
 * Copyright (C) 2014-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _GAZEBO_OPENSIMPOLYLINESHAPE_HH_
#define _GAZEBO_OPENSIMPOLYLINESHAPE_HH_

#include "gazebo/physics/PolylineShape.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
class OpensimMesh;

/// \addtogroup gazebo_physics_opensim
/// \{

/// \brief Opensim polyline shape
class GZ_PHYSICS_VISIBLE OpensimPolylineShape : public PolylineShape
{
  /// \brief Constructor
  /// \param[in] _parent Collision parent.
public:
  explicit OpensimPolylineShape(CollisionPtr _parent);

  /// \brief Destructor.
public:
  virtual ~OpensimPolylineShape() override;

  // Documentation inherited
public:
  virtual void Load(sdf::ElementPtr _sdf) override;

  // Documentation inherited
public:
  virtual void Init() override;

  /// \brief Opensim collsion mesh helper class.
private:
  OpensimMesh *opensimMesh;
};
/// \}
}
}
#endif
