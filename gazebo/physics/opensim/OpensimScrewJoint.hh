/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_SCREWJOINT_HH_
#define _OPENSIM_SCREWJOINT_HH_

#include <string>
#include "gazebo/physics/opensim/OpensimJoint.hh"
#include "gazebo/physics/ScrewJoint.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief A screw joint
class GZ_PHYSICS_VISIBLE OpensimScrewJoint : public ScrewJoint<OpensimJoint>
{
  /// \brief Constructor
  /// \param[in] _world Pointer to the Opensim world.
  /// \param[in] _parent Parent of the screw joint.
public:
  OpensimScrewJoint(BasePtr _parent, OpensimPhysics &_engine);

  /// \brief Destructor
public:
  virtual ~OpensimScrewJoint();

  // Documentation inherited.
protected:
  virtual void Load(sdf::ElementPtr _sdf) override;

  // Documentation inherited.
public:
  virtual void SetAxis(unsigned int _index,
                       const ignition::math::Vector3d &_axis) override;

  // Documentation inherited.
public:
  virtual void SetUpperLimit(unsigned int _index,
                           const double _angle) override;

  // Documentation inherited.
public:
  virtual void SetLowerLimit(unsigned int _index,
                          const double _angle) override;

  // Documentation inherited.
public:
  virtual double UpperLimit(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual double LowerLimit(unsigned int _index) const override;

  // Documentation inherited.
  //       public: virtual void SetThreadPitch(unsigned int _index,
  //                   double _threadPitch);

  // Documentation inherited.
public:
  virtual void SetThreadPitch(double _threadPitch) override;

  // Documentation inherited.
  //       public: virtual double GetThreadPitch(unsigned int /*_index*/);

  // Documentation inherited.
public:
  virtual double GetThreadPitch() override;

  // Documentation inherited.
public:
  virtual double GetVelocity(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual void SetVelocity(unsigned int _index, double _angle) override;

  // Documentation inherited.
public:
  virtual ignition::math::Vector3d GlobalAxis(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual double PositionImpl(unsigned int _index) const override;

  // Documentation inherited.
public:
  virtual bool SetParam(const std::string &_key,
                        unsigned int _index,
                        const boost::any &_value) override;

  // Documentation inherited.
public:
  virtual double GetParam(const std::string &_key,
                          unsigned int _index) override;

  // Documentation inherited.
protected:
  virtual void SetForceImpl(unsigned int _index, double _force) override;
};
/// \}
}
}
#endif
