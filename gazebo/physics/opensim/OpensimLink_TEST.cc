/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include <gtest/gtest.h>
#include <string>

#include "gazebo/physics/physics.hh"
#include "gazebo/physics/PhysicsEngine.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"


#include "gazebo/common/Mesh.hh"
#include "gazebo/common/MeshManager.hh"
#include "gazebo/common/Material.hh"
#include "gazebo/common/ColladaLoader.hh"
#include "gazebo/common/OBJExporter.hh"

#include "gazebo/test/ServerFixture.hh"
#include "test/util.hh"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/Model.hh"

#include "gazebo/msgs/msgs.hh"

using namespace gazebo;
using namespace physics;

class OpensimLink_TEST : public ServerFixture
{
protected:
  OpensimLink_TEST() : ServerFixture()
  {
    gazebo::common::Console::SetColored(false); // Because it mangles the output in jenkins.
  }
};


/////////////////////////////////////////////////
TEST_F(OpensimLink_TEST, MuscleLoadingSanityCheck)
{
  
  boost::filesystem::path path = PROJECT_SOURCE_PATH;
  path /= "test";
  path /= "models";
  path /= "opensim";
  gazebo::common::SystemPaths::Instance()->AddModelPaths(path.string());

  Load(std::string(PROJECT_SOURCE_PATH) + "/test/worlds/opensim/muscle_wrap_test.world", true, "opensim");
  
  WorldPtr world = get_world("default");
  ASSERT_TRUE(world != NULL);
  
  ModelPtr model = world->ModelByName("muscle_model");
  LinkPtr link1 = model->GetLink("link1");
  const ignition::math::Vector3d force(1.0, 2.0, 3.0);
  link1->SetForce(force);
  world->Step(3);
  const ignition::math::Pose3d expectedPose(0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
  const ignition::math::Pose3d pose(link1->WorldPose());
  static constexpr double ERR = 1e-5;
  gzwarn << pose.Pos()[0] << "\n";
  for (int i = 0; i < 3; ++i) {
    const double pi = pose.Pos()[i];
    const double epi = expectedPose.Pos()[i];
    EXPECT_NEAR(pi, epi, ERR) 
      << "Vectors pose.pos and expectedPose.pos differ at index " << i << ":\n"
      << pi << " is not equal to " << epi << ".\n";
  }
}

/////////////////////////////////////////////////
/// Main
int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
