/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

 /* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/World.hh"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimRayShape.hh"
#include "gazebo/physics/opensim/OpensimMultiRayShape.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimMultiRayShape::OpensimMultiRayShape(CollisionPtr _parent)
  : MultiRayShape(_parent)
{
  this->SetName("Opensim Multiray Shape");
  this->physicsEngine = boost::static_pointer_cast<OpensimPhysics>(
      this->collisionParent->GetWorld()->Physics());
}

//////////////////////////////////////////////////
OpensimMultiRayShape::~OpensimMultiRayShape()
{
}

//////////////////////////////////////////////////
void OpensimMultiRayShape::UpdateRays()
{
  std::vector< RayShapePtr >::iterator iter;
  for (iter = this->rays.begin(); iter != this->rays.end(); ++iter)
  {
    (*iter)->Update();
  }
}

//////////////////////////////////////////////////
void OpensimMultiRayShape::AddRay(const ignition::math::Vector3d &_start,
    const ignition::math::Vector3d &_end)
{
  MultiRayShape::AddRay(_start, _end);
  OpensimRayShapePtr ray(new OpensimRayShape(this->physicsEngine));
  ray->SetPoints(_start, _end);

  this->rays.push_back(ray);
}
