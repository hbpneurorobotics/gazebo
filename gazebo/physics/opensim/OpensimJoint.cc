/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include <string>

#include "gazebo/common/Exception.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/World.hh"

#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimJoint.hh"

using namespace gazebo;
using namespace physics;

// TODO for all accessors: We could keep things like position, velocity
// power, and so on saved in extra member variables, which are updated
// in the physics loop. This way we could return the data without having
// to wait for the entire duration of the physics loop.
// Analogously, we could buffer values for setter functions.


//////////////////////////////////////////////////
OpensimJoint::OpensimJoint(BasePtr _parent)
    : Joint(_parent), dataPtr(nullptr)
{
}

//////////////////////////////////////////////////
OpensimJoint::~OpensimJoint()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
void OpensimJoint::Load(sdf::ElementPtr _sdf)
{
  Joint::Load(_sdf);
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());

// I guess this won't need much change
#if 0
  // read must_be_loop_joint
  // \TODO: clean up
  if (_sdf->HasElement("physics") &&
    _sdf->GetElement("physics")->HasElement("opensim"))
  {
    this->mustBreakLoopHere = _sdf->GetElement("physics")->
      GetElement("opensim")->Get<bool>("must_be_loop_joint");
  }

  if (this->sdf->HasElement("axis"))
  {
    sdf::ElementPtr axisElem = this->sdf->GetElement("axis");
    if (axisElem->HasElement("dynamics"))
    {
      sdf::ElementPtr dynamicsElem = axisElem->GetElement("dynamics");

      if (dynamicsElem && dynamicsElem->HasElement("friction"))
      {
        sdf::ElementPtr frictionElem = dynamicsElem->GetElement("friction");
        gzlog << "joint friction not implemented\n";
      }
    }
  }

  if (this->sdf->HasElement("axis2"))
  {
    sdf::ElementPtr axisElem = this->sdf->GetElement("axis2");
    if (axisElem->HasElement("dynamics"))
    {
      sdf::ElementPtr dynamicsElem = axisElem->GetElement("dynamics");

      if (dynamicsElem && dynamicsElem->HasElement("friction"))
      {
        sdf::ElementPtr frictionElem = dynamicsElem->GetElement("friction");
        gzlog << "joint friction not implemented\n";
      }
    }
  }

  // Read old style
  //    <pose>pose on child</pose>
  // or new style

  // to support alternative unassembled joint pose specification
  // check if the new style of pose specification exists
  //    <parent>
  //      <link>parentName</link>
  //      <pose>parentPose</pose>
  //    </parent>
  // as compared to old style
  //    <parent>parentName</parent>
  //
  // \TODO: consider storing the unassembled format parent pose when
  // calling Joint::Load(sdf::ElementPtr)
#endif
}

//////////////////////////////////////////////////
void OpensimJoint::Reset()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  Joint::Reset();
}

//////////////////////////////////////////////////
void OpensimJoint::Fini()
{
  Joint::Fini();
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
bool OpensimJoint::CheckValidAxisAndDebugOut(unsigned int _axis, const char *_where) const
{
  if (_axis < this->DOF())
    return true;
  else
  {
    //TODO: The plattform tries to access axis 0 on joints that have 0 axes. But  why are there joints with zero axes?
    //gzwarn << "Access to invalid axis " << _axis << " of " << this->GetAngleCount() << " in " << _where << std::endl;
    return false;
  }
}

//////////////////////////////////////////////////
void OpensimJoint::CacheForceTorque()
{
// Assign values to reaction force memers of Gazebo.

#if 0
  // Note minus sign indicates these are reaction forces
  // by the Link on the Joint in the target Link frame.
  this->wrench.body1Force =
    -Vec3ToVector3(reactionForceOnParentBody);
  this->wrench.body1Torque =
    -Vec3ToVector3(reactionTorqueOnParentBody);

  this->wrench.body2Force =
    -Vec3ToVector3(reactionForceOnChildBody);
  this->wrench.body2Torque =
    -Vec3ToVector3(reactionTorqueOnChildBody);
#endif
}

//////////////////////////////////////////////////
LinkPtr OpensimJoint::GetJointLink(unsigned int _index) const
{
  LinkPtr result;

  if (_index == 0 || _index == 1)
  {
    OpensimLinkPtr opensimLink1 =
        boost::static_pointer_cast<OpensimLink>(this->childLink);

    OpensimLinkPtr opensimLink2 =
        boost::static_pointer_cast<OpensimLink>(this->parentLink);
  }

  return result;
}

//////////////////////////////////////////////////
bool OpensimJoint::AreConnected(LinkPtr _one, LinkPtr _two) const
{
  return ((this->childLink.get() == _one.get() &&
           this->parentLink.get() == _two.get()) ||
          (this->childLink.get() == _two.get() &&
           this->parentLink.get() == _one.get()));
}

//////////////////////////////////////////////////
void OpensimJoint::Detach()
{
  THROW_FUNCTION_NOT_IMPLEMENTED
  // this is not going to end well because the opensim model is still intact.
  this->childLink.reset();
  this->parentLink.reset();
}

//////////////////////////////////////////////////
void OpensimJoint::SetAnchor(unsigned int /*_index*/,
    const ignition::math::Vector3d & /*_anchor*/)
{
  gzerr << "OpensimJoint::SetAnchor:  Not implement in Opensim."
        << " Anchor is set during joint construction in OpensimPhysics.cc\n";
}

//////////////////////////////////////////////////
void OpensimJoint::SetDamping(unsigned int _index, const double _damping)
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());
    this->SetStiffnessDamping(_index, this->stiffnessCoefficient[_index],
                              _damping);
  }
}

//////////////////////////////////////////////////
void OpensimJoint::SetStiffness(unsigned int _index, const double _stiffness)
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());
    this->SetStiffnessDamping(_index, _stiffness,
                              this->dissipationCoefficient[_index]);
  }
}

//////////////////////////////////////////////////
void OpensimJoint::SetStiffnessDamping(unsigned int _index,
                                       double _stiffness, double _damping, double _reference)
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());
    this->stiffnessCoefficient[_index] = _stiffness;
    this->dissipationCoefficient[_index] = _damping;
    this->springReferencePosition[_index] = _reference;

    auto &force = GetPriv()->springCoordinateForce[_index];
    force.setStiffness(_stiffness);
    force.setViscosity(_damping);
    force.setRestLength(_reference);
  }
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimJoint::Anchor(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return ignition::math::Vector3d();
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimJoint::LinkForce(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return ignition::math::Vector3d();
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimJoint::LinkTorque(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return ignition::math::Vector3d();
}

//////////////////////////////////////////////////
bool OpensimJoint::SetParam(const std::string &/*_key*/,
    unsigned int /*_index*/, const boost::any &/*_value*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return false;
}

//////////////////////////////////////////////////
void OpensimJoint::SetUpperLimit(unsigned int _index, const double _angle)
{
  // this sets internal storage variables
  Joint::SetUpperLimit(_index, _angle);
}

//////////////////////////////////////////////////
void OpensimJoint::SetLowerLimit(unsigned int _index, const double _angle)
{
  Joint::SetLowerLimit(_index, _angle);
}

//////////////////////////////////////////////////
double OpensimJoint::UpperLimit(unsigned int _index) const
{
  return Joint::UpperLimit(_index);
}

//////////////////////////////////////////////////
double OpensimJoint::LowerLimit(unsigned int _index) const
{
  return Joint::LowerLimit(_index);
}

//////////////////////////////////////////////////
void OpensimJoint::SetAxis(unsigned int /*_index*/,
                           const ignition::math::Vector3d & /*_axis*/)
{
  // Opensim seems to handle setAxis improperly. It readjust all the pivot
  // points
  gzdbg << "SetAxis: setting axis is not yet implemented.  The axis are set "
        << " during joint construction in OpensimPhysics.cc for now.\n";
}

//////////////////////////////////////////////////
// void OpensimJoint::SetAxis(unsigned int _index, const ignition::math::Vector3d &/*_axis*/)
// {
// #if 0
//   ignition::math::Pose3d parentModelPose;
//   if (this->parentLink)
//     parentModelPose = this->parentLink->GetModel()->GetWorldPose();
//
//   // Set joint axis
//   // assuming incoming axis is defined in the model frame, so rotate them
//   // into the inertial frame
//   // TODO: switch so the incoming axis is defined in the child frame.
//   ignition::math::Vector3d axis = parentModelPose.rot.RotateVector(
//     this->sdf->GetElement("axis")->Get<ignition::math::Vector3d>("xyz"));
//
//   if (_index == 0)
//     this->sdf->GetElement("axis")->GetElement("xyz")->Set(axis);
//   else if (_index == 1)
//     this->sdf->GetElement("axis2")->GetElement("xyz")->Set(axis);
//   else
//     gzerr << "SetAxis index [" << _index << "] out of bounds\n";
// #endif
// }

//////////////////////////////////////////////////
void OpensimJoint::SetVelocity(unsigned int _index, double _rate)
{
  OpensimJointPrivate *priv = this->dataPtr.get();
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());
    // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
        *this->world->Physics()->GetPhysicsUpdateMutex());

    // TODO: tell the integrator that  a discontinuous change happened here!
    if (priv->engine->opensimPhysicsInitialized && priv->isOsimRepresentationInitialized() && priv->hasMobilizer())
    {
      const OpenSim::CoordinateSet &coordinateSet = priv->GetMob().joint->get_CoordinateSet();
      coordinateSet[0].setSpeedValue(priv->engine->GetPriv()->updWorkingStateRealizedToStage(SimTK::Stage::Velocity), _rate);
    }
    else
    {
      gzmsg << "OpensimJoint::SetVelocity(" << _index << "," << _rate << ") failed on joint " << this->GetName() << "! OpenSimJoint outboard link is NULL!" << std::endl;
    }
  }
}

//////////////////////////////////////////////////
double OpensimJoint::GetVelocity(unsigned int _index) const
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    const OpensimJointPrivate *priv = this->dataPtr.get();
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

    // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
       *this->world->Physics()->GetPhysicsUpdateMutex());

    if (priv->engine->opensimPhysicsInitialized && priv->isOsimRepresentationInitialized() && priv->hasMobilizer())
    {
      //NOTE: See GetAngleImpl
      //TODO: handle the case where the mobilizer direction is topologically reversed w.r.t. the gazebo joint.
      const OpenSim::CoordinateSet &coordinateSet = priv->GetMob().joint->get_CoordinateSet();
      double value = coordinateSet[0].getSpeedValue(priv->engine->GetPriv()->getWorkingStateRealizedToStage(SimTK::Stage::Velocity));
      return value;
    }
    else
      return 0;
  }
  return SimTK::NaN;
}

//////////////////////////////////////////////////
double OpensimJoint::GetAcceleration(unsigned int _index) const
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    const OpensimJointPrivate *priv = this->dataPtr.get();
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());
      // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
        *this->world->Physics()->GetPhysicsUpdateMutex());

    if (priv->engine->opensimPhysicsInitialized && priv->isOsimRepresentationInitialized() && priv->hasMobilizer())
    {
      //TODO: handle the case where the mobilizer direction is topologically reversed w.r.t. the gazebo joint.
      const OpenSim::CoordinateSet &coordinateSet = priv->GetMob().joint->get_CoordinateSet();
      double value = coordinateSet[0].getAccelerationValue(priv->engine->GetPriv()->getWorkingStateRealizedToStage(SimTK::Stage::Acceleration));
      return value;
    }
    else
      return 0.;
  }
  return SimTK::NaN;
}

//////////////////////////////////////////////////
double OpensimJoint::PositionImpl(unsigned int _index) const
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    const OpensimJointPrivate *priv = this->dataPtr.get();
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

    // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
        *this->world->Physics()->GetPhysicsUpdateMutex());

    if (priv->engine->opensimPhysicsInitialized && priv->isOsimRepresentationInitialized() && priv->hasMobilizer())
    {
      //NOTE: This is just a relatively thin wrapper around:
      //mobod.getOneQ(this->opensimPhysics->integ->getState(), _index);
      //TODO: handle the case where the mobilizer direction is topologically reversed w.r.t. the gazebo joint.
      const OpenSim::CoordinateSet &coordinateSet = priv->GetMob().joint->get_CoordinateSet();
      double value = coordinateSet[0].getValue(priv->engine->GetPriv()->getWorkingStateRealizedToStage(SimTK::Stage::Velocity));
      return value;
    }
    else
      return 0.0;
  }
  return SimTK::NaN;
}

//////////////////////////////////////////////////
void OpensimJoint::SetForceImpl(unsigned int _index, double _torque)
{
  // TODO: optionally disable the force if torque==0
  // FIXME: This implementation sets the force when it should correctly
  // add the values from successive calls. ApplyForce of
  // Bullet works this way. But unfortunately, Simbody is different.
  // We can work around the difference by subscribing to a "After physics"
  // event, zeroing the accumulated force.
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OpensimJointPrivate* priv = this->dataPtr.get();
    //gzdbg << __FUNCTION__ << " THREAD " << std::this_thread::get_id() << " on " << this->GetScopedName() << " F=" << _torque << std::endl;

    // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
        *this->world->Physics()->GetPhysicsUpdateMutex());

    //     OPENSIM_PHYSICS_DEBUG1({
    //       if (_torque > 1.e-8 && priv->externalCoordinateForce[_index].getCoordinate())
    //       {
    //         gzdbg << "Coordinate Force [" << priv->externalCoordinateForce[_index].getCoordinate()->getName() << "] = " << _torque << std::endl;
    //       }
    //     })
    priv->externalCoordinateForce[_index].setForce(_torque);
  }
}

//////////////////////////////////////////////////
void OpensimJoint::SetForce(unsigned int _index, double _force)
{
  double force = Joint::CheckAndTruncateForce(_index, _force);
  this->SetForceImpl(_index, force);
}

//////////////////////////////////////////////////
double OpensimJoint::GetForce(unsigned int _index)
{
  if (CheckValidAxisAndDebugOut(_index, __FUNCTION__))
  {
    OpensimJointPrivate *priv = this->dataPtr.get();
    OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

    // lock physics update mutex to ensure thread safety
    boost::recursive_mutex::scoped_lock lock(
        *this->world->Physics()->GetPhysicsUpdateMutex());

    return priv->externalCoordinateForce[_index].getForce();
#if 0
    return this->forceApplied[_index];
#endif
  }
  return 0;
}

//////////////////////////////////////////////////
JointWrench OpensimJoint::GetForceTorque(unsigned int /*_index*/)
{
  return this->wrench;
}

/////////////////////////////////////////////////
ignition::math::Vector3d OpensimJoint::GlobalAxis(unsigned int _index) const
{
  //   if (this->opensimPhysics &&
  //       this->opensimPhysics->opensimPhysicsStepped &&
  //       _index < this->GetAngleCount())
  //   {
  //     if (!this->mobod.isEmptyHandle())
  //     {
  //       const SimTK::Transform &X_OM = this->mobod.getOutboardFrame(
  //         this->opensimPhysics->integ->getState());
  //
  //       // express Z-axis of X_OM in world frame
  //       SimTK::Vec3 z_W(this->mobod.expressVectorInGroundFrame(
  //         this->opensimPhysics->integ->getState(), X_OM.z()));
  //
  //       return Vec3ToVector3(z_W);
  //     }
  //     else
  //     {
  //       gzerr << "Joint mobod not initialized correctly.  Returning"
  //             << " initial axis vector in world frame (not valid if"
  //             << " joint frame has moved). Please file"
  //             << " a report on issue tracker.\n";
  //       return this->GetAxisFrame(_index).RotateVector(
  //         this->GetLocalAxis(_index));
  //     }
  //   }
  //   else
  //   {
  //     if (_index >= this->GetAngleCount())
  //     {
  //       gzerr << "index out of bound\n";
  //       return math::Vector3(SimTK::NaN, SimTK::NaN, SimTK::NaN);
  //     }
  //     else
  //     {
  //       gzdbg << "GetGlobalAxis() sibmody physics engine not initialized yet, "
  //             << "use local axis and initial pose to compute "
  //             << "global axis.\n";
  //       // if local axis specified in model frame (to be changed)
  //       // switch to code below if issue #494 is to be addressed
  //       return this->GetAxisFrame(_index).RotateVector(
  //         this->GetLocalAxis(_index));
  //     }
  //   }
  return ignition::math::Vector3d();
}
