/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Mesh.hh"

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimMeshShape.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

#include "gazebo/common/SystemPaths.hh"
#include "gazebo/common/MeshManager.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/OBJExporter.hh"

#include "gazebo/physics/World.hh"

#include <random>


//////////////////////////////////////////////////
namespace gazebo { namespace physics { namespace opensim_internal
{

std::string SanitizeFilename(const std::string &s)
{
  static constexpr int NUM_CHARS = 1 << (sizeof(char)*8);
  char admissible[NUM_CHARS] = {};
  for (int c = 'a'; c <= 'z'; ++c)
    admissible[int(c)] = 1;
  for (int c = 'A'; c <= 'Z'; ++c)
    admissible[c] = 1;
  for (int c = '0'; c <= '9'; ++c)
    admissible[c] = 1;
  admissible[int('-')] = 1;
  admissible[int('_')] = 1;
  admissible[int('.')] = 1;
  std::string r(s);
  for (char &c: r)
  {
    if (!admissible[int(c)])
      c = '_';
  }
  return r;
}


std::string MakeTempFilePath(const Shape *shape, const Collision* collision)
{
  boost::filesystem::path tmpPath(gazebo::common::SystemPaths::Instance()->TmpPath());
  std::stringstream objFileNameStream;
  if (shape->GetName().empty())
  {
    if (!collision || collision->GetName().empty())
    {
      std::default_random_engine eng((std::random_device())());
      std::uniform_int_distribution<unsigned int> idis(0, std::numeric_limits<unsigned int>::max());
      objFileNameStream << "mesh_" << idis(eng) << ".obj";
    }
    else
    {
      objFileNameStream << collision->GetName() << ".obj";
    }
  }
  else
  {
    objFileNameStream << shape->GetName() << ".obj";
  }
  tmpPath = tmpPath / SanitizeFilename(objFileNameStream.str());
  return tmpPath.string();
}


void ConvertToSimTK(SimTK::PolygonalMesh &simtk_mesh, const common::Mesh &mesh, const ignition::math::Vector3d &scale_factor)
{
  SimTK::Array_<int> face; face.resize(3);
  for (unsigned int k = 0; k < mesh.GetSubMeshCount(); ++k)
  {
    const common::SubMesh *subMesh = mesh.GetSubMesh(k);
    if (subMesh->GetPrimitiveType() == common::SubMesh::TRIANGLES)
    {
      auto &vertices = subMesh->GetOriginalVertices();
      auto &indices = subMesh->GetOriginalIndices();
      auto s = subMesh->originalScaleFactor*scale_factor;

      int simtk_vertex_start_index = simtk_mesh.getNumVertices();
      for (unsigned int i = 0; i < vertices.size(); i++)
      {
        SimTK::Vec3 v(
          s[0]*vertices[i][0],
          s[1]*vertices[i][1],
          s[2]*vertices[i][2]
        );
        simtk_mesh.addVertex(v);
      }

      assert(indices.size() % 3 == 0);
      for (unsigned int i = 0; i < indices.size(); i+=3)
      {
        face[0] = indices[i+0] + simtk_vertex_start_index;
        face[1] = indices[i+1] + simtk_vertex_start_index;
        face[2] = indices[i+2] + simtk_vertex_start_index;
        simtk_mesh.addFace(face);
      }
    }
    else
    {
      gzwarn << "OpenSim physics encounted a submesh of unsupported primitives. Only triangles will be used." << std::endl;
    }
  }
}


}}} // namespaces

//////////////////////////////////////////////////
using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimMeshShape::OpensimMeshShape(CollisionPtr _parent)
    : MeshShape(_parent)
{
}

//////////////////////////////////////////////////
OpensimMeshShape::~OpensimMeshShape()
{
}

//////////////////////////////////////////////////
void OpensimMeshShape::Load(sdf::ElementPtr _sdf)
{
  MeshShape::Load(_sdf);
}

//////////////////////////////////////////////////
void OpensimMeshShape::Init()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  MeshShape::Init();
}

//////////////////////////////////////////////////
void OpensimMeshShape::AddToBody(OpensimPhysicsPrivate *_engine_priv, OpenSim::Body &_body)
{
  OpensimCollisionPtr collision = boost::dynamic_pointer_cast<OpensimCollision>(this->collisionParent);

  SimTK::Transform X_LC =
      Pose2Transform(
          collision->RelativePose());

  // convert to position/euler representation
  SimTK::Vec3 pos = X_LC.p();
  SimTK::Vec3 euler = X_LC.R().convertRotationToBodyFixedXYZ();

  SimTK::PolygonalMesh simtk_mesh;
  ConvertToSimTK(simtk_mesh, *this->mesh, this->Size());

  std::string osim_name = _engine_priv->CreateUniqueOsimNameFor(collision.get());
  try
  {
    this->osimGeometry = boost::make_shared<OpenSim::ContactMesh>(
      simtk_mesh, pos, euler, _body, osim_name);
  }
  catch (const std::exception &e)
  {
    std::cerr << "Error creating geometry for collision " << collision->GetName() << " (" << osim_name << "): " << e.what() << std::endl;
    throw;
  }
  _engine_priv->osimModel.addContactGeometry(this->osimGeometry.get());

  OPENSIM_PHYSICS_DEBUG1(gzdbg << "Created new OpenSim ContactMesh for "
                              << collision->GetName() << " (" << osim_name << ") "
                              << ", position = " << pos
                              << ", orientation = " << euler
                              << std::endl;)
}
