/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_COLLISION_HH_
#define _OPENSIM_COLLISION_HH_

#include <string>

#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/Collision.hh"
#include "gazebo/util/system.hh"

namespace OpenSim
{
class ContactGeometry;
}

namespace gazebo
{
namespace physics
{
/// \ingroup gazebo_physics
/// \addtogroup gazebo_physics_opensim Opensim Physics
/// \{

/// \brief Opensim collisions
class GZ_PHYSICS_VISIBLE OpensimCollision : public Collision
{
  /// \brief Constructor
public:
  OpensimCollision(LinkPtr _parent);

  /// \brief Destructor
public:
  virtual ~OpensimCollision() override;

  // Documentation inherited
public:
  virtual void Load(sdf::ElementPtr _ptr) override;

  // Documentation inherited
public:
  virtual void Init() override;

  // Documentation inherited
public:
  virtual void OnPoseChange() override;

  // Documentation inherited
public:
  virtual void SetCategoryBits(unsigned int _bits) override;

  // Documentation inherited
public:
  virtual void SetCollideBits(unsigned int _bits) override;

  // Documentation inherited
public:
  virtual ignition::math::AxisAlignedBox BoundingBox() const override;

  /// \brief The SimTK collision geometry.
private:
  OpenSim::ContactGeometry *osimGeometry;

public:
  struct SurfaceParameters
  {
    SurfaceParameters();
    double stiffness;
    double dissipation;
    double transitionVelocity;
    double staticFriction;
    double dynamicFriction;
    double viscousFriction;
  } surfaceParameters;

private:
  friend class OpensimPhysics;
  friend class OpensimPhysicsPrivate;
};
/// \}
}
}
#endif
