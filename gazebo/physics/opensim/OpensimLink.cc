/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/* Desc: Link class
 * Author: Nate Koenig
 * Date: 13 Feb 2006
 */

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"

#include "gazebo/physics/World.hh"

#include "gazebo/physics/opensim/OpensimCollision.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"

#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

namespace gazebo { namespace physics {

//////////////////////////////////////////////////
OpensimLink::OpensimLink(EntityPtr _parent, OpensimPhysics *_physics)
    : Link(_parent),
      dataPtr(boost::make_shared<OpensimLinkPrivate>(this, _physics))
{
 // NOTE: Simbody remains
//   this->physicsInitialized = false;
//   this->gravityMode = false;
//   this->gravityModeDirty = false;
//   this->mustBeBaseLink = false;

//   this->staticLinkDirty = false;
//   this->staticLink = false;

}

//////////////////////////////////////////////////
OpensimLink::~OpensimLink()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
void OpensimLink::Load(sdf::ElementPtr _sdf)
{
//   if (_sdf->HasElement("must_be_base_link"))
//     this->mustBeBaseLink = _sdf->Get<bool>("must_be_base_link");
//
//   this->SetKinematic(_sdf->Get<bool>("kinematic"));
//   this->SetGravityMode(_sdf->Get<bool>("gravity"));

  Link::Load(_sdf);

  // This is normally done in Link::Init. But we need it before
  // the link is added to the OpenSim model in Model::Init.
  this->SetRelativePose(this->sdf->Get<ignition::math::Pose3d>("pose"));
  this->SetInitialRelativePose(this->sdf->Get<ignition::math::Pose3d>("pose"));

  // at the end so we know the name of this thing
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
void OpensimLink::Init()
{
  /// \TODO: implement following
  // this->SetLinearDamping(this->GetLinearDamping());
  // this->SetAngularDamping(this->GetAngularDamping());
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  Link::Init();

//   ignition::math::Vector3d cogVec = this->inertial->GetCoG();

//   // Set the initial pose of the body
//
//   for (Base_V::iterator iter = this->children.begin();
//        iter != this->children.end(); ++iter)
//   {
//     if ((*iter)->HasType(Base::COLLISION))
//     {
//       OpensimCollisionPtr collision;
//       collision = boost::static_pointer_cast<OpensimCollision>(*iter);
//
//       ignition::math::Pose3d relativePose = collision->GetRelativePose();
//       relativePose.pos -= cogVec;
//     }
//   }
//
//   // Create a construction info object
//   // Create the new rigid body
//
//   // change link's gravity mode if requested by user
//   this->gravityModeConnection = event::Events::ConnectWorldUpdateBegin(
//     boost::bind(&OpensimLink::ProcessSetGravityMode, this));
//
//   // lock or unlock the link if requested by user
//   this->staticLinkConnection = event::Events::ConnectWorldUpdateEnd(
//     boost::bind(&OpensimLink::ProcessSetLinkStatic, this));
}

//////////////////////////////////////////////////
void OpensimLink::Fini()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
#if 0
  event::Events::DisconnectWorldUpdateBegin(this->gravityModeConnection);
  event::Events::DisconnectWorldUpdateEnd(this->staticLinkConnection);
#endif
  Link::Fini();
}

/////////////////////////////////////////////////////////////////////
void OpensimLink::Reset()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
  Link::Reset();
  // lock physics update mutex to ensure thread safety
  boost::recursive_mutex::scoped_lock lock(
    *this->world->Physics()->GetPhysicsUpdateMutex());
  this->dataPtr->RestoreDefaultValuesToInitialCoordinates();
}

/////////////////////////////////////////////////////////////////////
void OpensimLink::UpdateMass()
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimLink::SetGravityMode(bool _mode)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
#if 0
  // Set whether gravity affects this body
  if (!this->gravityModeDirty)
  {
    this->gravityModeDirty = true;
    this->gravityMode = _mode;
  }
  else
    gzerr << "Trying to SetGravityMode for link [" << this->GetScopedName()
          << "] before last setting is processed.\n";
#endif
}

//////////////////////////////////////////////////
void OpensimLink::ProcessSetGravityMode()
{
  WARN_FUNCTION_NOT_IMPLEMENTED
#if 0
  if (this->gravityModeDirty)
  {
    if (this->physicsInitialized)
    {
      this->sdf->GetElement("gravity")->Set(this->gravityMode);
      this->opensimPhysics->gravity.setBodyIsExcluded(
        this->opensimPhysics->integ->updAdvancedState(),
        this->masterMobod, !this->gravityMode);
      // realize system after changing gravity mode
      this->opensimPhysics->system.realize(
        this->opensimPhysics->integ->getState(), SimTK::Stage::Velocity);
      this->gravityModeDirty = false;
    }
    else
    {
      gzlog << "SetGravityMode [" << this->gravityMode
            << "], but physics not initialized, caching\n";
    }
  }
#endif
}

//////////////////////////////////////////////////
bool OpensimLink::GetGravityMode() const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
#if 0
  if (this->physicsInitialized)
  {
    return this->opensimPhysics->gravity.getBodyIsExcluded(
      this->opensimPhysics->integ->getState(), this->masterMobod);
  }
  else
  {
    gzlog << "GetGravityMode [" << this->gravityMode
          << "], but physics not initialized, returning cached value\n";
    return this->gravityMode;
  }
#endif
  return false;
}

//////////////////////////////////////////////////
void OpensimLink::SetSelfCollide(bool /*_collide*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimLink::OnPoseChange()
{
  Link::OnPoseChange();

#if 0
  if (!this->opensimPhysics->opensimPhysicsInitialized)
    return;

  if (this->masterMobod.isEmptyHandle())
    return;

  // debug
  // gzerr << "original: [" << Transform2Pose(
  //   this->masterMobod.getBodyTransform(
  //   this->opensimPhysics->integ->updAdvancedState()))
  //       << "]\n";

  if (!this->masterMobod.isGround())
  {
    if (this->masterMobod.getParentMobilizedBody().isGround())
    {
      /// If parent is ground:
      /// Setting 6 dof pose of a link works in opensim only if
      /// the inboard joint is a free joint to the ground for now.
      this->masterMobod.setQToFitTransform(
         this->opensimPhysics->integ->updAdvancedState(),
         Pose2Transform(this->GetWorldPose()));
    }
    else
    {
      gzerr << "SetWorldPose (OnPoseChange) for child links need testing.\n";
      /*
      /// If the inboard joint is not free, opensim tries to project
      /// target pose into available DOF's.
      /// But first convert to relative pose to parent mobod.
      ignition::math::Pose3d parentPose = Transform2Pose(
        this->masterMobod.getBodyTransform(
        this->opensimPhysics->integ->updAdvancedState()));
      ignition::math::Pose3d relPose = this->GetWorldPose() - parentPose;
      this->masterMobod.setQToFitTransform(
         this->opensimPhysics->integ->updAdvancedState(),
         Pose2Transform(relPose));
      */
    }
    // realize system after updating Q's
    this->opensimPhysics->system.realize(
      this->opensimPhysics->integ->getState(), SimTK::Stage::Position);
  }
#endif
}


//////////////////////////////////////////////////
void OpensimLink::SetLinkStatic(bool _static)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   if (!this->staticLinkDirty)
//   {
//     this->staticLinkDirty = true;
//     this->staticLink = _static;
//   }
//   else
//     gzerr << "Trying to SetLinkStatic before last setting is processed.\n";
}


//////////////////////////////////////////////////
void OpensimLink::ProcessSetLinkStatic()
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   if (this->masterMobod.isEmptyHandle())
//     return;
//
//   // check if inboard body is ground
//   if (this->staticLinkDirty &&
//       this->masterMobod.getParentMobilizedBody().isGround())
//   {
//     if (this->staticLink)
//       this->masterMobod.lock(
//        this->opensimPhysics->integ->updAdvancedState());
//     else
//       this->masterMobod.unlock(
//        this->opensimPhysics->integ->updAdvancedState());
//
//     // re-realize
//     this->opensimPhysics->system.realize(
//       this->opensimPhysics->integ->getAdvancedState(), SimTK::Stage::Velocity);
//   }
//   else
//   {
//     // gzerr << "debug: joint name: " << this->GetScopedName() << "\n";
//   }
//
//   this->staticLinkDirty = false;
}

//////////////////////////////////////////////////
void OpensimLink::SetEnabled(bool /*_enable*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimLink::SetLinearVel(const ignition::math::Vector3d & _vel)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   this->masterMobod.setUToFitLinearVelocity(
//     this->opensimPhysics->integ->updAdvancedState(),
//     Vector3ToVec3(_vel));
//   this->opensimPhysics->system.realize(
//     this->opensimPhysics->integ->getAdvancedState(), SimTK::Stage::Velocity);
}

//////////////////////////////////////////////////
namespace opensim_internal
{
/// \brief Encapsulates boiler plate code that runs before and after the actual work is done.
///
/// \param[in] link Link to operate on.
/// \param[in] stage The desired realization stage of the current state.
/// \param[in] accessor_name Appears in warning message
/// \param[in] default_ret Default return value
/// \param[in] f Callable doing the work. Use with c++11 lambdas. Takes body and state as arguments.
template<class R, class Func>
R MaybeExecuteGetterOnSimTKState(const OpensimLink &link, SimTK::Stage stage, const char* accessor_name, const R &default_ret, Func f)
{
  const OpensimLinkPrivate* priv = link.GetPriv();
  const OpensimPhysicsPrivate* engine_priv = priv->engine->GetPriv();

  // lock physics update mutex to ensure thread safety
  boost::recursive_mutex::scoped_lock lock(
    *priv->engine->GetPhysicsUpdateMutex());

  if (priv->engine->opensimPhysicsInitialized
      && priv->isOsimRepresentationInitialized())
  {
    const SimTK::MobilizedBody &body = priv->getSimTKBody();
    const SimTK::State &state = engine_priv->getWorkingStateRealizedToStage(stage);
    return f (body, state);
  }
  else
  {
    gzwarn << accessor_name << ": opensim physics not yet initialized\n";
    return default_ret;
  }
}
}

using opensim_internal::MaybeExecuteGetterOnSimTKState;

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldLinearVel(
  const ignition::math::Vector3d& _offset) const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Velocity, __FUNCTION__, ignition::math::Vector3d::Zero,
    [_offset](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    SimTK::Vec3 p_B(Vector3ToVec3(_offset));
    return Vec3ToVector3(
      body.findStationVelocityInGround(state, p_B));
  });
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldLinearVel(
  const ignition::math::Vector3d &_offset,
  const ignition::math::Quaterniond &_q) const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Velocity, __FUNCTION__, ignition::math::Vector3d::Zero,
    [_offset, _q](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    SimTK::Rotation R_WF(QuadToQuad(_q));
    SimTK::Vec3 p_F(Vector3ToVec3(_offset));
    SimTK::Vec3 p_W(R_WF * p_F);
    const SimTK::Rotation &R_WL = body.getBodyRotation(state);
    SimTK::Vec3 p_B(~R_WL * p_W);
    return Vec3ToVector3(
      body.findStationVelocityInGround(
        state, p_B));
  });
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldCoGLinearVel() const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Velocity, __FUNCTION__, ignition::math::Vector3d::Zero,
    [](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    SimTK::Vec3 station = body.getBodyMassCenterStation(
       state);
    return Vec3ToVector3(
      body.findStationVelocityInGround(
        state, station));
  });
}

//////////////////////////////////////////////////
void OpensimLink::SetAngularVel(const ignition::math::Vector3d &_vel)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   this->masterMobod.setUToFitAngularVelocity(
//     this->opensimPhysics->integ->updAdvancedState(),
//     Vector3ToVec3(_vel)
//   );
//   this->opensimPhysics->system.realize(
//     this->opensimPhysics->integ->getAdvancedState(), SimTK::Stage::Velocity);
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldAngularVel() const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Velocity, __FUNCTION__, ignition::math::Vector3d::Zero,
    [](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    SimTK::Vec3 w =
      body.getBodyAngularVelocity(state);
    return Vec3ToVector3(w);
  });
}

//////////////////////////////////////////////////
void OpensimLink::SetForce(const ignition::math::Vector3d &_force)
{
  // lock physics update mutex to ensure thread safety
   boost::recursive_mutex::scoped_lock lock(*GetPriv()->engine->GetPhysicsUpdateMutex());

  const SimTK::MobilizedBody &body = GetPriv()->getSimTKBody();
  const SimTK::Vec3 force(Vector3ToVec3(_force));
  const SimTK::SpatialVec spatialForce(SimTK::Vec3(0), force);
  OpensimPhysicsPrivate* physicsPrivate = GetPriv()->engine->GetPriv();
  SimTK::State &uws = physicsPrivate->osimModel.updWorkingState();
  GetPriv()->getDiscreteForces().setOneBodyForce(uws, body, spatialForce);
  physicsPrivate->osimModel.updMultibodySystem().realize(uws);
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldForce() const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Acceleration, __FUNCTION__, ignition::math::Vector3d::Zero,
    [](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    SimTK::Vec3 a = body.getBodyOriginAcceleration(state);
    SimTK::Real mass = body.getBodyMass(state);
    if (std::isinf(mass))
      mass = 0.;
    return Vec3ToVector3(mass*a);
  });
}

//////////////////////////////////////////////////
void OpensimLink::SetTorque(const ignition::math::Vector3d &_torque)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   SimTK::Vec3 t(Vector3ToVec3(_torque));
//
//   this->opensimPhysics->discreteForces.setOneBodyForce(
//     this->opensimPhysics->integ->updAdvancedState(),
//     this->masterMobod, SimTK::SpatialVec(t, SimTK::Vec3(0)));
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimLink::WorldTorque() const
{
  OPENSIM_PHYSICS_LOG_ACCESSOR_ON(this->GetScopedName());

  return MaybeExecuteGetterOnSimTKState(
    *this, SimTK::Stage::Acceleration, __FUNCTION__, ignition::math::Vector3d::Zero,
    [](const SimTK::MobilizedBody &body, const SimTK::State &state)
  {
    const SimTK::MassProperties &mp = body.getBodyMassProperties(state);
    if (!std::isinf(mp.getMass()))
    {
      // These quantities are expressed and measured w.r.t. ground frame.
      SimTK::Vec3 wdot = body.getBodyAngularAcceleration(state);
      SimTK::Vec3 w    = body.getBodyAngularVelocity(state);
      // Get inertia tensor, about COM, expressed in body frame.
      SimTK::Inertia I = mp.calcCentralInertia();
      // Express in ground frame.
      I.reexpressInPlace(body.getBodyRotation(state));
      // RHS is the derivative of the angular momentum.
      SimTK::Vec3 M = SimTK::cross(w, I*w)  + I*wdot;
      return Vec3ToVector3(M);
    }
    else
    {
      GZ_ASSERT(body.getBodyAngularAcceleration(state).scalarNormSqr() < 1.e-6, "If the mass is infinite we expect approximately zero acceleration.");
      // TODO: Perhaps there is a better way to do this? The issue is that there
      // can be non-zero torque on a static body.
      // Ofc the static body won't move due to it's infinite mass.
      // This calculation cannot handle the case because the torque is computed
      // from the acceleration!
      return ignition::math::Vector3d::Zero;
    }
  });
}

//////////////////////////////////////////////////
void OpensimLink::SetLinearDamping(double /*_damping*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimLink::SetAngularDamping(double /*_damping*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::AddForce(const ignition::math::Vector3d &_force)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
//   SimTK::Vec3 f(Vector3ToVec3(_force));
//
//   this->opensimPhysics->discreteForces.addForceToBodyPoint(
//     this->opensimPhysics->integ->updAdvancedState(),
//     this->masterMobod,
//     SimTK::Vec3(0), f);
}

/////////////////////////////////////////////////
void OpensimLink::AddRelativeForce(const ignition::math::Vector3d &/*_force*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::AddForceAtWorldPosition(const ignition::math::Vector3d &/*_force*/,
                                         const ignition::math::Vector3d &/*_pos*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::AddForceAtRelativePosition(const ignition::math::Vector3d &/*_force*/,
                  const ignition::math::Vector3d &/*_relpos*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimLink::AddLinkForce(const ignition::math::Vector3d &/*_force*/,
    const ignition::math::Vector3d &/*_offset*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::AddTorque(const ignition::math::Vector3d &/*_torque*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::AddRelativeTorque(const ignition::math::Vector3d &/*_torque*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
void OpensimLink::SetAutoDisable(bool /*_disable*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
bool OpensimLink::GetEnabled() const
{
  return true;
}

/////////////////////////////////////////////////
void OpensimLink::SetDirtyPose(const ignition::math::Pose3d &_pose)
{
  this->dirtyPose = _pose;
}

}} // namespace gazebo::physics