/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/common/Exception.hh"

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimBallJoint.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimBallJoint::OpensimBallJoint(BasePtr _parent, OpensimPhysics &_engine)
    : BallJoint<OpensimJoint>(_parent)
{
  this->dataPtr = boost::make_shared<OpensimJointPrivate>(this, &_engine);
}

//////////////////////////////////////////////////
OpensimBallJoint::~OpensimBallJoint()
{
}

//////////////////////////////////////////////////
void OpensimBallJoint::Load(sdf::ElementPtr _sdf)
{
  BallJoint<OpensimJoint>::Load(_sdf);
}

//////////////////////////////////////////////////
ignition::math::Vector3d OpensimBallJoint::Anchor(unsigned int /*_index*/) const
{
  return this->anchorPos;
}

/////////////////////////////////////////////////
void OpensimBallJoint::SetVelocity(unsigned int /*_index*/, double /*_angle*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

/////////////////////////////////////////////////
double OpensimBallJoint::GetVelocity(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return 0;
}

/////////////////////////////////////////////////
double OpensimBallJoint::GetAcceleration(unsigned int _index) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return 0;
}

/////////////////////////////////////////////////
ignition::math::Vector3d OpensimBallJoint::GetGlobalAxis(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return ignition::math::Vector3d();
}

/////////////////////////////////////////////////
ignition::math::Angle OpensimBallJoint::GetAngleImpl(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return ignition::math::Angle();
}

//////////////////////////////////////////////////
void OpensimBallJoint::SetForceImpl(unsigned int /*_index*/, double /*_torque*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
void OpensimBallJoint::SetAxis(unsigned int /*_index*/,
                               const ignition::math::Vector3d & /*_axis*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
}

//////////////////////////////////////////////////
double OpensimBallJoint::UpperLimit(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return 0.0;
}

//////////////////////////////////////////////////
double OpensimBallJoint::LowerLimit(unsigned int /*_index*/) const
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return 0.0;
}

//////////////////////////////////////////////////
bool OpensimBallJoint::SetHighStop(unsigned int /*_index*/,
                                   const ignition::math::Angle & /*_angle*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return false;
}

//////////////////////////////////////////////////
bool OpensimBallJoint::SetLowStop(unsigned int /*_index*/,
                                  const ignition::math::Angle & /*_angle*/)
{
  WARN_FUNCTION_NOT_IMPLEMENTED
  return false;
}
