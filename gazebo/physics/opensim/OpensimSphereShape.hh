/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_SPHERE_SHAPE_HH_
#define _OPENSIM_SPHERE_SHAPE_HH_

#include "gazebo/physics/SphereShape.hh"
#include "gazebo/physics/opensim/opensim_inc.h"
#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/util/system.hh"

namespace gazebo
{
  namespace physics
  {
    /// \ingroup gazebo_physics
    /// \addtogroup gazebo_physics_opensim Opensim Physics
    /// \{

    /// \brief Opensim sphere collision
    class GZ_PHYSICS_VISIBLE OpensimSphereShape : public SphereShape
    {
      /// \brief Constructor
      /// \param[in] _parent Collision parent pointer
      public: OpensimSphereShape(CollisionPtr _parent) : SphereShape(_parent) {}

      /// \brief Destructor
      public: virtual ~OpensimSphereShape();

      // Documentation inherited.
      public: virtual void SetRadius(double _radius) override;

      /// \brief Add the collision shape.
      ///
      /// Called by the physics engine so that shapes can add their
      /// add geometry to the OpenSim::Model contained in _engine.
      private: void AddToBody(OpensimPhysicsPrivate* _engine, OpenSim::Body &_body);

      /// \brief Keep a reference to the corresponding opensim object.
      private: boost::shared_ptr<OpenSim::ContactSphere> osimGeometry;

      // For easy access from the physics engine
      private:
        friend class OpensimPhysics;
        friend class OpensimPhysicsPrivate;
    };
    /// \}
  }
}
#endif
