/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#include "gazebo/physics/World.hh"
#include "gazebo/physics/Gripper.hh"
#include "gazebo/transport/Publisher.hh"
#include "gazebo/physics/opensim/OpensimModel.hh"
#include "gazebo/physics/opensim/OpensimPhysics.hh"
#include "gazebo/physics/opensim/OpensimLink.hh"
#include "gazebo/physics/opensim/OpensimPhysicsPrivate.hh"
#include "gazebo/common/CommonIface.hh"

using namespace gazebo;
using namespace physics;

//////////////////////////////////////////////////
OpensimModel::OpensimModel(BasePtr _parent, OpensimPhysics *_engine)
  : Model(_parent), engine(_engine)
{
}

//////////////////////////////////////////////////
OpensimModel::~OpensimModel()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
void OpensimModel::Load(sdf::ElementPtr _sdf)
{
  OPENSIM_PHYSICS_LOG_CALL();

  if (_sdf->HasElement("muscles"))
  {
    sdf::ElementPtr muscles_sdf = _sdf->GetElement("muscles");
    GZ_ASSERT(muscles_sdf != nullptr, "Really, the element is there so the pointer must be obtained!");

    try
    {
      std::string tmp = muscles_sdf->Get<std::string>();
      // Searches everywhere except where the model file is. Unless the work directory
      // is the directory where the model is. This is not very good.
      // TODO: more robust file search.
      this->muscle_definition_filename = common::find_file(tmp);
      if (this->muscle_definition_filename.empty())
        throw std::invalid_argument("File " + this->muscle_definition_filename + "not found.");
    }
    catch(const std::exception &e)
    {
      gzerr << "Muscle specification tag found but a muscle specification file could not be read because: " << e.what() << std::endl;
    }
  }

  Model::Load(_sdf);
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());
}

//////////////////////////////////////////////////
void OpensimModel::Init()
{
  GZ_ASSERT(this->engine != nullptr, "Are you trying to use a Fini(she)d model?!");

  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());

  Model::Init(); // recurses into nested models

  // This should limit invocations to the root model only.
  if (this->IsRootModel())
    this->engine->AddModelHierarchyToOpensimSystem(this);
}

//////////////////////////////////////////////////
void OpensimModel::Fini()
{
  OPENSIM_PHYSICS_LOG_CALL_ON(this->GetScopedName());

  // Protect against multiple Fini Calls!
  if (this->engine)
  {
    if (this->IsRootModel())
      this->engine->RemoveModelHierarchyFromOpensimSystem(*this);
    this->muscles.clear();
    this->owned_osim_forces.clear();
    this->engine = nullptr;
  }

  // Model::Fini clears containers with links, joints, etc.
  Model::Fini();
}

//////////////////////////////////////////////////
const Muscle_V& OpensimModel::GetMuscles() const
{
  return this->muscles;
}

//////////////////////////////////////////////////
void OpensimModel::Reset()
{
  Model::Reset();
}
//////////////////////////////////////////////////
// void OpensimModel::FillMsg(msgs::Model &_msg)
// {
//   // rebuild opensim state
//   // this needs to happen before this->joints are used
//   physics::OpensimPhysicsPtr opensimPhysics =
//     boost::dynamic_pointer_cast<physics::OpensimPhysics>(
//       this->GetWorld()->Physics());
//   if (opensimPhysics)
//     opensimPhysics->InitModel(this);
//
//   Model::FillMsg(_msg);
// }

bool OpensimModel::IsRootModel() const
{
  BasePtr p = this->GetParent();
  GZ_ASSERT(p && (p->GetType()==BASE || p->HasType(MODEL)), "Parent must not be null. It should be of type BASE representing the world, or be another model.");
  return p->GetType()==BASE;
}
