/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/* Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
*/

#ifndef _OPENSIM_MUSCLE_HH_
#define _OPENSIM_MUSCLE_HH_

#include "gazebo/physics/opensim/OpensimTypes.hh"
#include "gazebo/physics/opensim/opensim_inc.h"
#include <ignition/math/Vector3.hh>


namespace OpenSim
{

class ExternalActuatorController;

}

namespace gazebo
{
namespace physics
{

/// @brief Thin wrapper around Opensim Muscles.
///
/// See OpensimPhysics::LoadMuscleDefinitionsAndDoSanityCheck
class GZ_PHYSICS_VISIBLE OpensimPathActuator
{
  /// @brief Internal ctor. Do not use.
  /// Called by OpensimPhysics, after muscle definition file has been loaded.
  public:
    OpensimPathActuator(OpenSim::PathActuator *_osimMuscle, const std::string &_name, OpensimPhysics &_engine);

  /// @brief You don't want to copy construct this.
    OpensimPathActuator(const OpensimPathActuator &) = delete;

  /// @brief You don't want to copy assign this.
    OpensimPathActuator& operator=(const OpensimPathActuator &) = delete;

  /// @brief dtor.
  public: virtual ~OpensimPathActuator();

  /// @brief Persistent activation signal for muscles.
  public: void SetActivationValue(double value);

  /// @brief Return the last setting.
  public: double GetActivationValue() const;

  /// @brief Its name
  public: std::string GetName() const;

  /// @brief Wraps the OpenSim::Muscle member of the same name.
  public: double GetOptimalFiberLength() const;

  /// @brief Wraps the OpenSim::Muscle member of the same name.
  public: double GetTendonSlackLength() const;

  /// @brief Wraps the OpenSim::Muscle member of the same name.
  public: double GetForce() const;

  /// @brief Wraps the OpenSim::Muscle member of the same name.
  public: double GetLength() const;

  /// @brief Wraps the OpenSim::Muscle member of the same name.
  public: double GetLengtheningSpeed() const;

  /// @brief The number of nodes in the path
  public: int GetPathNodeCount() const;

  /// @brief Or is it a path actuator that is not a muscle.
  public: bool IsMuscle() const;

  /// @brief Return the current path after wrapping.
  ///
  /// That is, the original path specified in the sdf/.osim file
  //  which has potentially been modified by WrapObjects and
  //  possibly other mechanisms. Note that the number of segments
  //  can change depending on the supplied state. Note also that
  //  the result is cached, and that the cache may be recomputed
  //  by this function, incuring a large computational cost.
  public: void GetCurrentWorldPath(std::vector<ignition::math::Vector3d> &_positions) const;

  ////////////////////////////////////////////////////////////////////

  /// @brief Opensim Muscle.
  ///
  /// Takes ownership after a muscles are loaded from xml.
  private: OpenSim::PathActuator *osimActuator;

  /// @brief Its name as it appears in topics and so on.
  ///
  /// Differs from the name of the opensim class instance.
  private: std::string name;

  /// @brief The controler.
  ///
  /// Constructed here. Forwards its control value as activation signal to the osimMuscle.
  private: boost::shared_ptr<OpenSim::ExternalActuatorController> osimController;

  /// @brief Remove the OpenSim Force and Controller instances. Should be private.
  public: void RemoveFrom(OpenSim::Model &osimModel);

  /// @brief A reference to the physics engine implementation instance.
  private: OpensimPhysicsPrivate* engine_priv;

  private: friend class OpensimPhysics;
};


} // namespace physics
} // namespace gazebo

#endif
