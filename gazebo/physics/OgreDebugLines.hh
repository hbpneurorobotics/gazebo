/***************************************************************************

This source file is part of OGREBULLET
(Object-oriented Graphics Rendering Engine Bullet Wrapper)
For the latest info, see http://www.ogre3d.org/phpBB2addons/viewforum.php?f=10

Copyright (c) 2007 tuan.kuranes@gmail.com (Use it Freely, even Statically, but have to contribute any changes)



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/
#ifndef _OgreBulletCollisions_DEBUGLines_H_
#define _OgreBulletCollisions_DEBUGLines_H_

#include <OGRE/Ogre.h>

namespace gazebo
{
  namespace physics
  {
    //------------------------------------------------------------------------------------------------
    class DebugLines
    {
    public:
      DebugLines(Ogre::SceneManager* scm = NULL);
      ~DebugLines();

      inline void addLine(const Ogre::Vector3 &start, const Ogre::Vector3 &end, const Ogre::Vector3& color = Ogre::Vector3(1,0,0))
      {
        //clear();

        _lines.push_back(start);
        _lines.push_back(end);
        _lineColors.push_back(color);
      }

      inline void addLine(Ogre::Real startX, Ogre::Real startY, Ogre::Real startZ,
                          Ogre::Real endX, Ogre::Real endY, Ogre::Real endZ)
      {
        addLine(Ogre::Vector3(startX, startY, startZ),
                Ogre::Vector3(endX, endY, endZ), Ogre::Vector3(1,0,0));
      }

      inline void addPoint(const Ogre::Vector3 &pt, const Ogre::Vector3& color = Ogre::Vector3(1,0,0))
      {
        _points.push_back(pt);
        _pointColors.push_back(color);
      }

      inline void addPoint(Ogre::Real x, Ogre::Real y, Ogre::Real z)
      {
        addPoint(Ogre::Vector3(x, y, z), Ogre::Vector3(1,0,0));
      }

      void beginDraw();
      void draw();
      void endDraw();

      void clear(bool force = false);

      inline void setActive(bool active) { _active = active; }
      inline bool isActive() const { return _active; }

      inline const std::vector<Ogre::Vector3>& getLines() const { return _lines; }
      inline const std::vector<Ogre::Vector3>& getPoints() const { return _points; }
      inline const std::vector<Ogre::Vector3>& getTriangles() const { return _triangles; }

      inline const std::vector<Ogre::Vector3>& getLineColors() const { return _lineColors; }
      inline const std::vector<Ogre::Vector3>& getPointColors() const { return _pointColors; }
      inline const std::vector<Ogre::Vector3>& getTriangleColors() const { return _triangleColors; }

    protected:
      std::vector<Ogre::Vector3> _lines;
      std::vector<Ogre::Vector3> _lineColors;

      std::vector<Ogre::Vector3> _points;
      std::vector<Ogre::Vector3> _pointColors;

      std::vector<Ogre::Vector3> _triangles;
      std::vector<Ogre::Vector3> _triangleColors;

      Ogre::ManualObject* mPoints;
      Ogre::ManualObject* mLines;

      Ogre::SceneManager* mSceneManager;
      Ogre::SceneNode* mDebugParentNode;

      bool _drawn;
      bool _active;

      static bool _materials_created;
    };
  }
}
#endif //_OgreBulletCollisions_DEBUGLines_H_

