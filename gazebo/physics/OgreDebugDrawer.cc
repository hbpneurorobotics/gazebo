/***************************************************************************

This source file is part of OGREBULLET
(Object-oriented Graphics Rendering Engine Bullet Wrapper)

Copyright (c) 2007 tuan.kuranes@gmail.com (Use it Freely, even Statically, but have to contribute any changes)



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#include "OgreDebugDrawer.hh"

#include "gazebo/common/Console.hh"

using namespace gazebo::physics;
using namespace Ogre;

//------------------------------------------------------------------------------------------------
DebugDrawer::DebugDrawer(Ogre::SceneManager *scm)
  : DebugLines(scm),
    mDebugMode(0)
{
  gzdbg << "DebugDrawer::DebugDrawer(" << scm << ")" << std::endl;
}
//------------------------------------------------------------------------------------------------
DebugDrawer::~DebugDrawer()
{
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::reportErrorWarning(const char* warningString)
{
  Ogre::LogManager::getSingleton().getDefaultLog()->logMessage(warningString);
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setDrawAabb(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_DrawAabb;
  }
  else
  {
    mDebugMode &= ~DBG_DrawAabb;
  }
}

//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesDrawAabb() const { return (mDebugMode & DBG_DrawAabb) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesDrawWireframe() const { return (mDebugMode & DBG_DrawWireframe) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesDrawFeaturesText() const { return (mDebugMode & DBG_DrawFeaturesText) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesDrawContactPoints() const { return (mDebugMode & DBG_DrawContactPoints) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesNoDeactivation() const { return (mDebugMode & DBG_NoDeactivation) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesNoHelpText() const { return (mDebugMode & DBG_NoHelpText) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesDrawText() const { return (mDebugMode & DBG_DrawText) != 0; }
//------------------------------------------------------------------------------------------------
bool DebugDrawer::doesProfileTimings() const { return (mDebugMode & DBG_ProfileTimings) != 0; }
//------------------------------------------------------------------------------------------------
void DebugDrawer::setDrawWireframe(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_DrawWireframe;
  }
  else
  {
    mDebugMode &= ~DBG_DrawWireframe;
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setDrawFeaturesText(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_DrawFeaturesText;
  }
  else
  {
    mDebugMode &= ~DBG_DrawFeaturesText;
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setDrawContactPoints(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_DrawContactPoints;
  }
  else
  {
    mDebugMode &= ~DBG_DrawContactPoints;
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setNoDeactivation(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_NoDeactivation;
  }
  else
  {
    mDebugMode &= ~DBG_NoDeactivation;
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setNoHelpText(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_NoHelpText;
  }
  else
  {
    mDebugMode &= ~DBG_NoHelpText;
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::setDrawText(bool enable)
{
  if (enable)
  {
    mDebugMode |= DBG_DrawText;
  }
  else
  {
    mDebugMode &= ~DBG_DrawText;
  }
}

void DebugDrawer::drawTriangle(const Ogre::Vector3& v0,const Ogre::Vector3& v1,const Ogre::Vector3& v2,const Ogre::Vector3& color, double /*alpha*/)
{
  if (mDebugMode & DBG_DrawWireframe)
  {
    _triangles.push_back(Ogre::Vector3(v0.x, v0.y, v0.z));
    _triangles.push_back(Ogre::Vector3(v1.x, v1.y, v1.z));
    _triangles.push_back(Ogre::Vector3(v2.x, v2.y, v2.z));
    _triangleColors.push_back(Ogre::Vector3(color.x, color.y, color.z));
  }
}

//------------------------------------------------------------------------------------------------
void DebugDrawer::drawAabb(const Ogre::Vector3 &from, const Ogre::Vector3 &to, const Ogre::Vector3 &color)
{
  if (mDebugMode & DBG_DrawAabb)
  {
    Ogre::Vector3 halfExtents = (to-from)* 0.5f;
    Ogre::Vector3 center = (to+from) *0.5f;
    int i,j;

    Ogre::Vector3 edgecoord(1.f,1.f,1.f),pa,pb;
    for (i=0;i<4;i++)
    {
      for (j=0;j<3;j++)
      {
        pa = Ogre::Vector3(edgecoord[0]*halfExtents[0], edgecoord[1]*halfExtents[1],
            edgecoord[2]*halfExtents[2]);
        pa+=center;

        int othercoord = j%3;
        edgecoord[othercoord]*=-1.f;
        pb = Ogre::Vector3(edgecoord[0]*halfExtents[0], edgecoord[1]*halfExtents[1],
            edgecoord[2]*halfExtents[2]);
        pb+=center;

        drawLine(pa,pb,color);
      }
      edgecoord = Ogre::Vector3(-1.f,-1.f,-1.f);
      if (i<3)
        edgecoord[i]*=-1.f;
    }
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::drawLine(const Ogre::Vector3 &from, const Ogre::Vector3 &to, const Ogre::Vector3 &color)
{
  if (mDebugMode > 0)
  {
    //std::cout << "DebugDrawer::drawLine() Ogre::Vector3(" << from << "," << to << "," << color << ")" << std::endl;
    addLine(from, to, color);
  }
}
//------------------------------------------------------------------------------------------------
void DebugDrawer::drawContactPoint(const Ogre::Vector3 &PointOnB, const Ogre::Vector3 &normalOnB,
                                   Ogre::Real distance, int lifeTime, const Ogre::Vector3 &color)
{
  if (mDebugMode & DBG_DrawContactPoints)
  {
    addPoint(PointOnB, color);
  }
}

void DebugDrawer::drawContactPoint(const Ogre::Vector3 &pointOnB, const Ogre::Vector3 &normalOnB,
                              double distance, int lifeTime, const Ogre::Vector3 &color)
{
  drawContactPoint(Ogre::Vector3(pointOnB.x, pointOnB.y, pointOnB.z),
                   Ogre::Vector3(normalOnB.x, normalOnB.y, normalOnB.z),
                   distance, lifeTime, Ogre::Vector3(color.x, color.y, color.z));
}


//------------------------------------------------------------------------------------------------

bool DebugDrawer::frameStarted( const Ogre::FrameEvent& evt )
{
  gzdbg << "DebugDrawer::frameStarted()" << std::endl;
  DebugLines::draw();

  DebugLines::endDraw();

  return true;
}

bool DebugDrawer::frameEnded( const Ogre::FrameEvent& evt )
{
  gzdbg << "DebugDrawer::frameEnded()" << std::endl;
  DebugLines::beginDraw();
  return true;
}
