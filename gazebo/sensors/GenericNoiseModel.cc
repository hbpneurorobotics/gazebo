/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifdef _WIN32
  // Ensure that Winsock2.h is included before Windows.h, which can get
  // pulled in by anybody (e.g., Boost).
  #include <Winsock2.h>
#endif

#include "gazebo/common/Assert.hh"
#include "gazebo/common/Console.hh"
#include "gazebo/rendering/ogre_gazebo.h"
#include "gazebo/rendering/Camera.hh"
#include "gazebo/sensors/GenericNoiseModel.hh"

#include "gazebo/physics/World.hh"
#include "gazebo/physics/Link.hh"
#include <gazebo/physics/physics.hh>
#include "RandomExtension.hh"

#ifdef BUILD_WITH_LIBNOISE
  #include <iostream>
  #include <noise/noise.h>
#endif

namespace gazebo
{
  // We'll create an instance of this class for each camera, to be used to
  // inject random values on each render call.
  class GenericNoiseCompositorListener
    : public Ogre::CompositorInstance::Listener
  {
    /// \brief Constructor, setting mean and standard deviation.

    public: GenericNoiseCompositorListener(double _mean, double _stddev, std::string _typefunction, rendering::CameraPtr _camera):
        mean(_mean), stddev(_stddev), typefunction(_typefunction), camera(_camera) {}
    
    /// \brief Callback that OGRE will invoke for us on each render call
    /// \param[in] _passID OGRE material pass ID.
    /// \param[in] _mat Pointer to OGRE material.
    public: virtual void notifyMaterialRender(unsigned int _passId,
                                              Ogre::MaterialPtr &_mat)
    {
      GZ_ASSERT(!_mat.isNull(), "Null OGRE material");
      // modify material here (wont alter the base material!), called for
      // every drawn geometry instance (i.e. compositor render_quad)

     if (this->camera->GetBoolChangeNoiseModel())
     {
         std::map<std::string, std::tuple<std::string, std::string, std::string, bool>> newParameters = this->camera->GetChangeNoiseModelProperties();
         bool update_param = std::get<3>(newParameters.begin()->second);
         if (update_param)
         {
           if(camera->ScopedName() == newParameters.begin()->first)
           {
              if (std::get<0>(newParameters.begin()->second) !="gaussian")
              {
                typefunction = std::get<0>(newParameters.begin()->second) ;
              	this->mean = std::stod(std::get<1>(newParameters.begin()->second));
              	this->stddev = std::stod(std::get<2>(newParameters.begin()->second));
                this->camera->SetBoolChangeNoiseModel(false);
              	gzdbg << "The new parameters (meand and stdev) for Generic Noise Model is set" << std::endl;
              } else
              {
                 this->mean = 0.0;
 		 this->stddev = 0.0;
              }
            }
         }
      }

      Ogre::Vector3 offsets(0.0,0.0,0.0);
      if (typefunction == "gamma")
         offsets = (ignition::math::RandomExtension::DblGamma(0.1, 1.0),
                               ignition::math::RandomExtension::DblGamma(0.1, 1.0),
                               ignition::math::RandomExtension::DblGamma(0.1, 1.0));
      else if (typefunction == "cauchy")
         offsets = (ignition::math::RandomExtension::DblCauchy(0.0, 1.0),
                               ignition::math::RandomExtension::DblCauchy(0.0, 1.0),
                               ignition::math::RandomExtension::DblCauchy(0.0, 1.0));
      else if (typefunction == "lognormal")
         offsets = (ignition::math::RandomExtension::DblLognormal(0.0, 1.0),
                               ignition::math::RandomExtension::DblLognormal(0.0, 1.0),
                               ignition::math::RandomExtension::DblLognormal(0.0, 1.0));
      else if (typefunction == "fisher")
         offsets = (ignition::math::RandomExtension::DblFisher(4.0, 4.0),
                               ignition::math::RandomExtension::DblFisher(4.0, 4.0),
                               ignition::math::RandomExtension::DblFisher(4.0, 4.0));
      else if (typefunction == "squared")
         offsets = (ignition::math::RandomExtension::DblSquared(2),
                               ignition::math::RandomExtension::DblSquared(2),
                               ignition::math::RandomExtension::DblSquared(2));
      // These calls are setting parameters that are declared in two places:
      // 1. media/materials/scripts/gazebo.material, in
      //    fragment_program Gazebo/GenericCameraNoiseFS
      // 2. media/materials/scripts/camera_noise_Generic_fs.glsl
      Ogre::Technique *technique = _mat->getTechnique(0);
      GZ_ASSERT(technique, "Null OGRE material technique");
      Ogre::Pass *pass = technique->getPass(_passId);
      GZ_ASSERT(pass, "Null OGRE material pass");
      Ogre::GpuProgramParametersSharedPtr params =
          pass->getFragmentProgramParameters();
      GZ_ASSERT(!params.isNull(), "Null OGRE material GPU parameters");

      params->setNamedConstant("offsets", offsets);
      params->setNamedConstant("mean", static_cast<Ogre::Real>(this->mean));
      params->setNamedConstant("stddev", static_cast<Ogre::Real>(this->stddev));
    }
    
    /// \brief Mean that we'll pass down to the GLSL fragment shader.
    private: double mean;
    /// \brief Standard deviation that we'll pass down to the GLSL fragment
    /// shader.
    private: double stddev;

    private: std::string typefunction;

    private: rendering::CameraPtr camera;
  };
}  // namespace gazebo

using namespace gazebo;
using namespace sensors;

//////////////////////////////////////////////////
GenericNoiseModel::GenericNoiseModel()
  : Noise(Noise::GENERIC),
    mean(0.0),
    stdDev(0.0),
    bias(0.0),
    precision(0.0),
    quantized(false),
    typefunction("gamma"),
    active_library(false)
{
}

//////////////////////////////////////////////////
GenericNoiseModel::~GenericNoiseModel()
{
}

//////////////////////////////////////////////////
void GenericNoiseModel::Load(sdf::ElementPtr _sdf, const std::vector<std::string> &_sensorNames)
{
  Noise::Load(_sdf , _sensorNames);

  this->worldName = _sensorNames[0];
  this->parentSensorName = _sensorNames[1];
  this->sensorName = _sensorNames[2];

  this->mean = _sdf->Get<double>("mean");
  this->stdDev = _sdf->Get<double>("stddev");
  // Sample the bias
  double biasMean = 0;
  double biasStdDev = 0;
  if (_sdf->HasElement("bias_mean"))
    biasMean = _sdf->Get<double>("bias_mean");
  if (_sdf->HasElement("bias_stddev"))
    biasStdDev = _sdf->Get<double>("bias_stddev");
  this->bias = ignition::math::Rand::DblNormal(biasMean, biasStdDev);

  // With equal probability, we pick a negative bias (by convention,
  // rateBiasMean should be positive, though it would work fine if
  // negative).
  if (ignition::math::Rand::DblUniform() < 0.5)
    this->bias = -this->bias;

  /// \todo Remove this, and use Noise::Print. See ImuSensor for an example
  gzlog << "applying Generic noise model with mean " << this->mean
    << ", stddev " << this->stdDev
    << ", bias " << this->bias << std::endl;

  if (_sdf->HasElement("function"))
  {
       std::string Name_fun = _sdf->Get<std::string>("function");
      if (Name_fun == "gamma" || Name_fun == "cauchy" || Name_fun == "lognormal" || Name_fun == "fisher" || Name_fun == "squared" || Name_fun == "libnoise" )
      {
          this->typefunction = Name_fun;
          gzdbg << "Distribution function is " << this->typefunction << std::endl;
      }
      else
          gzerr << "Distribution function is not valid and it set as Gamma distribution  "  << std::endl;
  }

}

//////////////////////////////////////////////////
void GenericNoiseModel::Fini()
{
  Noise::Fini();
}

//////////////////////////////////////////////////
double GenericNoiseModel::ApplyImpl(double _in, double _dt)
{

  if (!linkSensor)
  {
     physics::WorldPtr world = physics::get_world(this->worldName);
     linkSensor = boost::dynamic_pointer_cast<gazebo::physics::Link>(world->EntityByName(this->parentSensorName));
  }
  if (linkSensor)
  {
      std::map<std::string, std::tuple<std::string, std::string, std::string, bool>> parametersROSservice ;
      parametersROSservice = linkSensor->UpdateNoiseParameter(this->worldName + "::" + this->parentSensorName + "::" + this->sensorName);
      bool updated_param = std::get<3>(parametersROSservice.begin()->second);
         if (updated_param)
         {
              if (std::get<0>(parametersROSservice.begin()->second) == "gaussian")
              {
                     this->GenericIsActive = true;
              } else
              {
                     this->typefunction = std::get<0>(parametersROSservice.begin()->second);
                     this->GenericIsActive = false;
              }
              this->mean = std::stod(std::get<1>(parametersROSservice.begin()->second));
              this->stdDev = std::stod(std::get<2>(parametersROSservice.begin()->second));
         }
   }
 
  double whiteNoise;
  double output;
  if (this->GenericIsActive)
  {
        GaussianNoiseModel newNoiseModel;
        output = newNoiseModel.calculateGaussianNoiseModel(this->mean, this->stdDev, _in, _dt);
  } else
  {
    if (this->typefunction == "gamma")
    {
      if (this->mean <= 0.0 || this->stdDev <= 0.0)
         gzerr << "For Gamma function, the first parameter must be positive"  << std::endl;
      else
         whiteNoise = ignition::math::RandomExtension::DblGamma(this->mean, this->stdDev);
    }
    else if (this->typefunction == "cauchy")
    {
      if (this->stdDev <= 0.0)
         gzerr << "For Cauchy function, the second parameter must be positive"  << std::endl;
      else
         whiteNoise = ignition::math::RandomExtension::DblCauchy(this->mean, this->stdDev);
    }
    else if (this->typefunction == "lognormal")
    {
      if (this->stdDev <= 0.0)
         gzerr << "For Lognormal function, the second parameter must be positive"  << std::endl;
      else
         whiteNoise = ignition::math::RandomExtension::DblLognormal(this->mean, this->stdDev);
    }
    else if (this->typefunction == "fisher")
    {
      if (this->stdDev <= 0.0 || this->mean <= 0.0 )
         gzerr << "For Fisher function, the first and second parameters must be positive"  << std::endl;
      else
         whiteNoise = ignition::math::RandomExtension::DblFisher(this->mean, this->stdDev);
    }
    else if (this->typefunction == "squared")
    {
      if (this->mean <= 0.0 )
         gzerr << "For Squared function, the first parameter must be positive"  << std::endl;
      else
         whiteNoise = ignition::math::RandomExtension::DblSquared(this->mean);
    }
    output = _in + this->bias + whiteNoise;
  }
  return output;
}

//////////////////////////////////////////////////
double GenericNoiseModel::GetMean() const
{
  return this->mean;
}

//////////////////////////////////////////////////
double GenericNoiseModel::GetStdDev() const
{
  return this->stdDev;
}

//////////////////////////////////////////////////
double GenericNoiseModel::GetBias() const
{
  return this->bias;
}

//////////////////////////////////////////////////
double GenericNoiseModel::calculateGenericNoiseModel(std::string &_newTypeFunction, double &_newmean, double &_newstdDev, double _in, double _dt) 
{
  this->typefunction = _newTypeFunction;
  this->mean = _newmean;
  this->stdDev = _newstdDev;
  return ApplyImpl(_in, _dt);
}

//////////////////////////////////////////////////
void GenericNoiseModel::Print(std::ostream &_out) const
{
  _out << "Generic noise, mean[" << this->mean << "], "
    << "stdDev[" << this->stdDev << "] "
    << "bias[" << this->bias << "] "
    << "precision[" << this->precision << "] "
    << "quantized[" << this->quantized << "]";
}

//////////////////////////////////////////////////
ImageGenericNoiseModel::ImageGenericNoiseModel()
  : GenericNoiseModel()
{
}

//////////////////////////////////////////////////
ImageGenericNoiseModel::~ImageGenericNoiseModel()
{
}

//////////////////////////////////////////////////
void ImageGenericNoiseModel::Load(sdf::ElementPtr _sdf, const std::vector<std::string> &_sensorNames)
{
  GenericNoiseModel::Load(_sdf, _sensorNames);
}

/////////////////////////////////////////////////
void ImageGenericNoiseModel::SetCamera(rendering::CameraPtr _camera)
{
  GZ_ASSERT(_camera, "Unable to apply Generic noise, camera is NULL");
  if (this->typefunction == "libnoise")
  { 
     this->active_library = true;
     #ifdef BUILD_WITH_LIBNOISE 
     #else
        gzerr << "LibNoise Library is not added.\n";
     #endif
  }
  else
  {
    this->genericNoiseCompositorListener.reset(new
        GenericNoiseCompositorListener(this->mean, this->stdDev, this->typefunction, _camera));
    this->genericNoiseInstance =
      Ogre::CompositorManager::getSingleton().addCompositor(
      _camera->OgreViewport(), "CameraNoise/Generic");
    this->genericNoiseInstance->setEnabled(true);
    this->genericNoiseInstance->addListener(
       this->genericNoiseCompositorListener.get());
   }
}

double ImageGenericNoiseModel::CalculateNoiseValue(double x, double y, double z)
{
  double value(0.0);
#ifdef BUILD_WITH_LIBNOISE
  noise::module::Perlin myModule;
  value = myModule.GetValue (x, y, z);
#endif
  return value;
}

//////////////////////////////////////////////////
void ImageGenericNoiseModel::Fini()
{
  GenericNoiseModel::Fini();
  if (this->genericNoiseCompositorListener)
  {
    this->genericNoiseInstance->removeListener(
      this->genericNoiseCompositorListener.get());
  }
}

//////////////////////////////////////////////////
void ImageGenericNoiseModel::Print(std::ostream &_out) const
{
  _out << "Image Generic noise, mean[" << this->mean << "], "
    << "stdDev[" << this->stdDev << "] "
    << "bias[" << this->bias << "] "
    << "precision[" << this->precision << "] "
    << "quantized[" << this->quantized << "]";
}

//////////////////////////////////////////////////
std::string ImageGenericNoiseModel::GetTypeFunction(sdf::ElementPtr _sdf)
{
  std::string Name_fun;
  if (_sdf->HasElement("function"))
  {
       Name_fun = _sdf->Get<std::string>("function");

  }
  return Name_fun;
}

//////////////////////////////////////////////////
std::string ImageGenericNoiseModel::GetNameFunction()
{
  return this->typefunction;
}

//////////////////////////////////////////////////
bool ImageGenericNoiseModel::GetTypeNoise()
{
  return this->active_library;
}
