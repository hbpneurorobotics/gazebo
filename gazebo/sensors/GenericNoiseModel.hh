/*
 * Copyright (C) 2012-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef _GAZEBO_GENERIC_NOISE_MODEL_HH_
#define _GAZEBO_GENERIC_NOISE_MODEL_HH_

#include <vector>
#include <string>

#include <sdf/sdf.hh>

#include "gazebo/rendering/RenderTypes.hh"
#include "gazebo/sensors/Noise.hh"
#include "gazebo/util/system.hh"

#include "gazebo/physics/Link.hh"
#include "gazebo/sensors/GaussianNoiseModel.hh"

namespace Ogre
{
  class CompositorInstance;
}

namespace gazebo
{
  class GenericNoiseCompositorListener;

  namespace sensors
  {
    /// \class GenericNoiseModel
    /// \brief Generic noise class
    class GAZEBO_VISIBLE GenericNoiseModel : public Noise
    {
        /// \brief Constructor.
        public: GenericNoiseModel();

        /// \brief Destructor.
        public: virtual ~GenericNoiseModel();

        // Documentation inherited.
        public: virtual void Load(sdf::ElementPtr _sdf, const std::vector<std::string> &_sensorNames = {"", "", ""});

        // Documentation inherited.
        public: virtual void Fini();

        // Documentation inherited.
        public: double ApplyImpl(double _in, double _dt);

        /// \brief Accessor for mean.
        /// \return Mean of Generic noise.
        public: double GetMean() const;

        /// \brief Accessor for stddev.
        /// \return Standard deviation of Generic noise.
        public: double GetStdDev() const;

        /// \brief Accessor for bias.
        /// \return Bias on output.
        public: double GetBias() const;

        public: double calculateGenericNoiseModel(std::string &_newTypeFunction, double &_newmean, double &_newstdDev, double _in, double _dt) ;
 
        /// Documentation inherited
        public: virtual void Print(std::ostream &_out) const;

        //typedef boost::mt19937 GeneratorType;
        //typedef boost::normal_distribution<double> NormalRealDist;
        //typedef boost::variate_generator<GeneratorType&, NormalRealDist > NRealGen;

        /// \brief If type starts with Generic, the mean of the distribution
        /// from which we sample when adding noise.
        protected: double mean;

        /// \brief If type starts with Generic, the standard deviation of the
        /// distribution from which we sample when adding noise.
        protected: double stdDev;

        /// \brief If type starts with Generic, the bias we'll add.
        protected: double bias;

        /// \brief If type==Generic_QUANTIZED, the precision to which
        /// the output signal is rounded.
        protected: double precision;

        /// \brief True if the type is Generic_QUANTIZED
        protected: bool quantized;

        protected: std::string typefunction;
   
        protected: bool active_library;

        protected: std::string parentSensorName;
        protected: std::string worldName;
        protected: std::string sensorName;
        private: gazebo::physics::LinkPtr linkSensor;
        protected: bool GenericIsActive = false;
        protected: std::string newfunction;
    };

    /// \class GenericNoiseModel
    /// \brief Generic noise class for image sensors
    class GAZEBO_VISIBLE ImageGenericNoiseModel : public GenericNoiseModel
    {
      /// \brief Constructor.
      public: ImageGenericNoiseModel();

      /// \brief Destructor.
      public: virtual ~ImageGenericNoiseModel();

      // Documentation inherited.
      public: virtual void Load(sdf::ElementPtr _sdf, const std::vector<std::string> &_sensorNames = {"", "", ""});

      // Documentation inherited.
      public: virtual void Fini();

      // Documentation inherited.
      public: virtual void SetCamera(rendering::CameraPtr _camera);

      /// Documentation inherited
      public: virtual void Print(std::ostream &_out) const;

      /// \brief Generic noise compositor.
      public: Ogre::CompositorInstance *genericNoiseInstance;

      /// \brief Generic noise compositor listener
      public: boost::shared_ptr<GenericNoiseCompositorListener>
        genericNoiseCompositorListener;
      
      public: double CalculateNoiseValue(double x, double y, double z);
 
      public: std::string GetTypeFunction(sdf::ElementPtr _sdf);

      public: std::string GetNameFunction();

      public: bool GetTypeNoise();

    };
    /// \}
  }
}

#endif
