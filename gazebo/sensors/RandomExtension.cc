/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include <sys/types.h>
#include <ctime>

#ifdef _WIN32
  #include <process.h>
  #define getpid _getpid
#else
  #include <unistd.h>
#endif

#include "RandomExtension.hh"

using namespace ignition;
using namespace math;

//////////////////////////////////////////////////
double RandomExtension::DblGamma(double _mean, double _sigma)
{
  GammaRealDist d(_mean, _sigma);
  return d(RandGenerator());
}

//////////////////////////////////////////////////
double RandomExtension::DblCauchy(double _mean, double _sigma)
{
  CauchyRealDist d(_mean, _sigma);
  return d(RandGenerator());
}

//////////////////////////////////////////////////
double RandomExtension::DblLognormal(double _mean, double _sigma)
{
  LognormalRealDist d(_mean, _sigma);

  return d(RandGenerator());
}

//////////////////////////////////////////////////
double RandomExtension::DblFisher(double _mean, double _sigma)
{
  FisherRealDist d(_mean, _sigma);

  return d(RandGenerator());
}

//////////////////////////////////////////////////
double RandomExtension::DblSquared(double _mean)
{
  SquaredRealDist d(_mean);

  return d(RandGenerator());
}


//////////////////////////////////////////////////
GeneratorType &RandomExtension::RandGenerator()
{
  static GeneratorType randGenerator(Seed());
  return randGenerator;
}

//////////////////////////////////////////////////
uint32_t &RandomExtension::SeedMutable()
{
  // We don't seed with time for the cases when two processes are started the
  // same time (this mostly happens with launch scripts that start a server
  // and gui simultaneously).
  static uint32_t seed = std::random_device {}();
  return seed;
}


//////////////////////////////////////////////////
void RandomExtension::Seed(unsigned int _seed)
{
  std::seed_seq seq{_seed};
  SeedMutable() = _seed;
  RandGenerator().seed(seq);
}

//////////////////////////////////////////////////
unsigned int RandomExtension::Seed()
{
  return SeedMutable();
}