#ifndef _ERROR_HH_
#define _ERROR_HH_

#include <cstdint>

namespace gazebo
{
    namespace error
    {
        // Error's id for ~/error topic
        enum Id : unsigned int
        {
            OPENSIM_COLLISION_MESH = 1
        };

        // Seconds to wait for gzbridge's connection
        static const double WAIT_GZBRIDGE_SECONDS = 5.0;
    }
}

#endif