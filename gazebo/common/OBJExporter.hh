#ifndef _GAZEBO_OBJEXPORTER_HH_
#define _GAZEBO_OBJEXPORTER_HH_

#include <map>
#include <string>
#include <vector>

#include "gazebo/common/MeshExporter.hh"
#include "gazebo/util/system.hh"
#include "ignition/math/Matrix4.hh"

class TiXmlElement;

namespace gazebo
{
  namespace common
  {
    class GZ_COMMON_VISIBLE OBJExporter: public MeshExporter
    {
      /// \brief Constructor
      public: OBJExporter();

      /// \brief Destructor
      public: virtual ~OBJExporter();

      /// \param[in] _mesh Pointer to the mesh to be exported
      /// \param[in] _filename Exported file's path and name
      /// \param[in] _exportTextures True to export texture images to
      /// '../materials/textures' folder - ignored by this OBJExporter
      public: virtual void Export(const Mesh *_mesh,
          const std::string &_filename, bool _exportTextures);

      /// \brief Transformation matrix applied to the vertices. Defaults to identity.
      public: ignition::math::Matrix4d extra_model_transform;
    };
  }
}

#endif // _GAZEBO_OBJEXPORTER_HH_
