This repository is part of the Neurorobotics Platform software
Copyright (C) Human Brain Project
https://neurorobotics.net

The Human Brain Project is a European Commission funded project
in the frame of the [Horizon2020 FET Flagship plan](http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships).

This work has received funding from the European Union’s Horizon 2020 Framework Programme for Research and Innovation under the Specific Grant Agreement No. 720270 (Human Brain Project SGA1), and the Specific Grant Agreement No. 785907 (Human Brain Project SGA2), and under the Specific Grant Agreement No. 945539 (Human Brain Project SGA3).


You are free to clone this repository and amend its code with respect to
the license that you find in the root folder.

## Submitting a pull request

To submit code changes (pull requests) as a person external to the project, do as follows.

0. Log in to Bitbucket
1. Fork the project: "+" button, "Fork this repository".
2. Clone the forked project (eg. ```git clone git@bitbucket.org:[USERNAME]/[REPOSITORY].git```)
3. Create a branch. Give it an explicit name referring a ticket number in [Jira](https://hbpneurorobotics.atlassian.net)
4. Do your code changes
5. Commit your changes. 
  **Make sure** your commit message starts with ```[<ticket_number>]```.
   Example: "[NUIT-10] My new feature"
6. ```git push```
7. Click on the url provided on the console output for the previous command to create the pull request
8. A core developer will eventually review your pull request and approve or comment it


Gazebo - A dynamic multi-robot simulator
----------------------------------------

This is the Gazebo simulator.  Gazebo simulates multiple robots in a
3D environment, with extensive dynamic interaction between objects.

  http://gazebosim.org

Installation
------------
Instructions are located at

  http://gazebosim.org/install

Gazebo cmake parameters available at configuring time:

 - BUILD_TESTING (boot) [default False]
   Include the test suite compilation in the default make call (make all).
 - ENABLE_DIAGNOSTICS
   If this is defined, it will enable diagnostic timers using the macros
   from Diagnostics.hh (see also the standalone diagnostics example):
   DIAG_TIMER_START("name")
   DIAG_TIMER_LAP("name")
   DIAG_TIMER_STOP("name")
 - USE_HOST_CFLAGS (bool) [default True]
   Check the building machine for supported compiler optimizations and use
   them to build the software.
 - USE_UPSTREAM_CFLAGS (bool) [default True]
   Use the recommend gazebo developers compiler optimizations flags.
 - USE_EXTERNAL_TINYXML (bool) [default True]
   Use external copy of tinyxml during the build.
 - USE_EXTERNAL_TINYXML2 (bool) [default True]
   Use external copy of tinyxml2 during the build.
 - USE_LOW_MEMORY_TEST (bool) [default False]
   Use reduced version of tests which need less quantity of RAM memory
   available.
 - FORCE_GRAPHIC_TESTS_COMPILATION (bool) [default False]
   Ignore system checks to look for graphic and acceleration support and
   compile all the test suites.
 - ENABLE_SCREEN_TESTS (bool) [default True]
   Enable or disable tests that need screen rendering to run properly.
   Headless machines or machines with the screen turned off should set this to
   False.
 - USE_PCH (bool) [default False]
   Use GNU Precompiled Headers. Only works with the gnu compiler.

 - ENABLE_TESTS_COMPILATION (DEPRECATED)
   The new behaviour is to call 'make tests' explicitly to compile the test
   suite. Calling 'make' or 'make all' won't compile the tests.


Uninstallation
--------------
Read the uninstallation instructions (http://gazebosim.org/uninstall) in the
online manual for generic instructions.  For most people, the following
sequence will suffice (might need sudo if it installed in /usr):

  $ make uninstall (inside the gazebo/build directory)
